# Execution Instructions
1. Create the environment configuration
    1. copy the src/config/template.env.development.local file.
    2. paste it at the top level of the project (with the other .env files)
    3. remove the "template" from the name (.env.development.local)
    4. Edit the stored variables
        * REACT_APP_BACKEND_API should be host url for your local backend. This is defaulted to localhost:3000 but it may be different, the console output for starting the backend should tell you what is it running on.
        * REACT_APP_ENCRYPTION_KEY should be your secret key. You need this to decript the other environment variables.
2. Open a terminal and run "npm install" within the project
3. See Project Details below for running commands

# Testing Instructions

## Writing
All unit tests should be put in the tests directory with an identical path to the component that it is testing. for example if a components path is /src/components/sample/sample_component.tsx then its unit tests should be at /src/tests/components/sample/sample_component_test.tsx. Mocks that might be used by multiple tests should be defined in the /src/setupTest.Tsx file. That file is exeucuted before any tests are.

## technologies
All tests are written using jest mocks and react test renderer.

## Executing
all tests named blah.test.tsx can be executed by running 'npm test'

# Environment Encryption
The sensitive values in the .env files are encrypted using Advanced Encryption Standard (AES) with a single private key. All developers should have a copy private key that should NOT be stored in the repository. All sensitive passwords/strings/information that shouldn't be in the repository should be encrypted using AES with the private key and stored in the environment files. 

## Environment files in react
.env is the base file<br/>
.env.development is used with npm start<br/>
.env.test is used with npm test<br/>
.env.production is used with npm run start<br/>

variables in .env.(environment) will always override ones in the .env file
variables in .env...local will override regular env files at the same level

### version control 
all environment files can be pushed (thats why everything needs to be encrypted) except the .local files. Those will have your secrete enctyption key and should not be pushed (the gitignore ignores all .local files).

### adding environment variables
all environment variables added need to be prefaced with REACT_APP_ in order to be unsed in react code.

## CryptoJS
decryption in this project is done with the cryptojs library. For decrypting an environment variable use the decrypt function in the /src/components/environment/EnvironmentManager.tsx file.

For encrypting a string use the tool at https://cryptii.com/pipes/aes-encryption with the following steps 
<img src="./src/assets/encryptionSteps.PNG"
     alt="Markdown Monster icon" />
Replacing the "text to encrypt" with your own text and the private key with the team private key. The text output on the right is what should be placed in the environment file.

# Project Details
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
