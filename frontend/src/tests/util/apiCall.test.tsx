import { APICall, Endpoint } from "../../util/apiCall";

const BACKEND_URL = process.env.REACT_APP_BACKEND_API;

const mockFetchResponse = Promise.resolve({
    json: () => Promise.resolve({})
});
jest.spyOn(global, 'fetch').mockImplementation(() => mockFetchResponse);

describe("APICall helper class", () => {

    describe("When building a GET ONE request", () => {
        it("builds the correct URL for getting one item", () => {
            var getOne = APICall.to(Endpoint.Beverages).suppressError();
            getOne.getById(10);
            var getOneUrl = getOne.getUrl();
            
            expect(getOneUrl).toBe(`${BACKEND_URL}/beverages/10`);
        });
    });

    describe("when building a GET ALL request", () => {
        it("builds the correct URL for getting the whole list", () => {
            var getAll = APICall.to(Endpoint.PourSizes).suppressError();
            getAll.get();
            var getAllUrl = getAll.getUrl();

            expect(getAllUrl).toBe(`${BACKEND_URL}/pour_sizes`);
        });
    });

    describe("when building a POST request", () => {
        it("builds the correct URL for a post request", () => {
            var post = APICall.to(Endpoint.Groupings).suppressError();
            post.post({});
            var postUrl = post.getUrl();

            expect(postUrl).toBe(`${BACKEND_URL}/groupings`);
        });
    });

    describe("when building a PATCH request", () => {
        it("builds the correct URL for a patch request", () => {
            var patch = APICall.to(Endpoint.Menus).suppressError();
            patch.patch(123, {});
            var patchUrl = patch.getUrl();

            expect(patchUrl).toBe(`${BACKEND_URL}/menus/123`);
        });
    });

    describe("when building a DELETE request", () => {
        it("builds the correct URL for deleting only that item", () => {
            var deleteApi = APICall.to(Endpoint.BeverageTypes).suppressError();
            deleteApi.delete(534);
            var deleteUrl = deleteApi.getUrl();

            expect(deleteUrl).toBe(`${BACKEND_URL}/beverage_types/534`);
        });
    });
});