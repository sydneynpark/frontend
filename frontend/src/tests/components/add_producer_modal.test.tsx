import AddProducerModal from '../../components/add_producer_modal';
import React from 'react';
import TestRenderer, { ReactTestInstance, act } from 'react-test-renderer';
import ReactDOM from 'react-dom';
import { mount, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Enzyme from 'enzyme';


var fetchMock = jest.spyOn(global, 'fetch');
const testFunc = () => {
    //Left empty purposefully
}

Enzyme.configure({ adapter: new Adapter() })

describe("Add Producer Modal", () => {

  beforeEach(() => {
    fetchMock.mockClear();
  })

  test("Missing data should result in fail", () => {
    let test_state =
    {
      name: "",
      description: "",
    };

    const expected_post_data_missing = new FormData();
    expected_post_data_missing.append("name", test_state.name);
    expected_post_data_missing.append("description", test_state.description);
    const wrapper = shallow(<AddProducerModal show={true} successfulCreation={testFunc} unsuccessfulCreation={testFunc} cancel={testFunc}/>);

    wrapper.setState(test_state);
    wrapper.find('#save-button').simulate('click', {preventDefault:()=>{}});
    expect(fetchMock).not.toHaveBeenLastCalledWith('http://TestURL/producers/',
        { method: 'POST',  body: expected_post_data_missing   });
    });

    test("Valid data should not error", () => {
    let test_state =
    {
      name: "TestProducerA",
      description: "TestProducerDescriptionA",
    };

    const expected_post_data_valid = new FormData();
    expected_post_data_valid.append("name", test_state.name);
    expected_post_data_valid.append("description", test_state.description);

    const wrapper = shallow(<AddProducerModal show={true} successfulCreation={testFunc} unsuccessfulCreation={testFunc} cancel={testFunc}/>);
    wrapper.setState(test_state);
    wrapper.find('#save-button').simulate('click', {preventDefault:()=>{}});
    expect(fetchMock).toHaveBeenLastCalledWith('http://TestURL/producers/',
        { method: 'POST',  body: expected_post_data_valid   });
    });

  test("All data is sent in request", async () =>  {
    //Assemble
        let test_state =
        {
          name: "TestProducerA",
          description: "TestProducerDescriptionA"
        };

        async function callback() {
          return test_state;
        }
        var apiResponse: any = {status: 200, json: callback};
        fetchMock.mockImplementation((thing1: any, thing2: any) => Promise.resolve(apiResponse));
        const wrapper = shallow(<AddProducerModal show={true} successfulCreation={testFunc} unsuccessfulCreation={testFunc} cancel={testFunc}/>);
        wrapper.setState(test_state);

        const expected_post_data = new FormData();
        expected_post_data.append("name", test_state.name);
        expected_post_data.append("description", test_state.description);

        //Act
        await act(async () => {
          wrapper.find('#save-button').simulate('click', {preventDefault:()=>{}});
        });

        //Assert
        expect(fetchMock).toHaveBeenLastCalledWith('http://TestURL/producers/',
            { method: 'POST',  body: expected_post_data   });
    });



   });
