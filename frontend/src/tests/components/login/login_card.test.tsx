import LoginCard from '../../../components/login/login_card';
import TestRenderer, { ReactTestInstance, act } from 'react-test-renderer';
import React from 'react';
import {Redirect} from 'react-router-dom';

//create spys
var fetchMock = jest.spyOn(global, 'fetch');
var sessionMock = jest.spyOn(Storage.prototype, 'setItem');
window.location.reload = jest.fn();
var mockSetUser = jest.fn((user: any) => {});

//create mocks
jest.mock('../../../components/sessions/userSession.tsx', () => {
  return {
    __esModule: true,
    default: {setUser(user: any){
      mockSetUser(user);
    }},
  }
});

//initialize tested module
const testRenderer = TestRenderer.create(
  <LoginCard></LoginCard>
);
const testInstance = testRenderer.root;


describe("Login Card", () => {

  beforeEach(() => {
    fetchMock.mockClear();
    sessionMock.mockClear();
    window.location.reload.mockClear();
    mockSetUser.mockClear();
  })

  it("renders all it's components correctly", () => {
    //check that the card content is rendered
    expect(testInstance.findByProps({className: "card-content"})).toBeDefined();
    //check that both the google buttons are present
    expect(testInstance.findByProps({className: "login-button"})).toBeDefined();
    expect(testInstance.findByProps({className: "create-button"})).toBeDefined();
    //check that popups arent visible
    expect(testInstance.findByProps({dialogTitle: "Account Already Exists"}).props.open).toBeFalsy();
    expect(testInstance.findByProps({dialogTitle: "Account Doesn't Exist"}).props.open).toBeFalsy();
    expect(testInstance.findByProps({buttonText: "OK"}).props.open).toBeFalsy();
  });

  describe("when loggin in", () => {
    var loginButton: ReactTestInstance;

    beforeEach(() => {
      //Assemble
      loginButton = testInstance.findByProps({className: "login-button"})
    });

    it("opens a dialog when an account doesn't exists", async () => {
      //Assemble
      var response: any = {status: 403, json: () =>  {}};
      fetchMock.mockImplementation((thing1: any, thing2: any) => Promise.resolve(response));

      //Act
      await act(async () => {
        loginButton.props.onSuccess({code: "its a me mario"});
      });

      //Assert
      expect(fetchMock).toBeCalledTimes(1);
      expect(testInstance.findByProps({dialogTitle: "Account Doesn't Exist"}).props.open).toBeTruthy();
    });

    it("opens an error dialog when it cant connect to the backend", async () =>  {
        //Assemble
        var response: any = {status: 404, json: () =>  {}};
        fetchMock.mockImplementation((thing1: any, thing2: any) => Promise.resolve(response));
      
        //Act
        await act(async () => {
          loginButton.props.onSuccess({code: "its a me mario"});
        });
      
        //Assert
        expect(fetchMock).toBeCalledTimes(1);
        var errorPopup = testInstance.findByProps({buttonText: 'OK'});
        expect(errorPopup.props.open).toBeTruthy();
        expect(errorPopup.props.dialogTitle).toEqual("Connection Error");
        expect(errorPopup.props.dialogDescription).toEqual("An unknown error occurred in communication with the API.");
    });

    it("opens an error dialog when the google login fails", async () => {
        //Assemble
        var response = {error: "idpiframe_initialization_failed", description: "doesnt matter"}
        
        //Act
        await act(async () => {
          loginButton.props.onFailure(response);
        });
        
        //Assert
        var errorPopup = testInstance.findByProps({buttonText: 'OK'});
        expect(errorPopup.props.open).toBeTruthy();
        expect(errorPopup.props.dialogTitle).toEqual("idpiframe_initialization_failed");
        expect(errorPopup.props.dialogDescription).toEqual("Initialization of the Google Auth API failed, try enabling third party cookies.");

        //Cleanup
        await act(async () => {
          errorPopup.props.onAccept();
        });
    });

    it("starts a user session on the frontend and redirects to the home page when successful", async () =>  {
        //Assemble
        var userInfo = {
          given_name: "hello",
          family_name: "world",
          email: "mario@world.com",
          sub: "ahhhhh"
        }
        async function callback() {
          return userInfo
        }
        var apiResponse: any = {status: 200, json: callback};
        fetchMock.mockImplementation((thing1: any, thing2: any) => Promise.resolve(apiResponse));
      
        //Act
        await act(async () => {
          loginButton.props.onSuccess({code: "its a me mario"});
        });
      
        //Assert
        expect(fetchMock).toBeCalledTimes(1);
        expect(sessionMock).toBeCalledTimes(1);
        expect(sessionMock).toBeCalledWith("user", JSON.stringify({
          firstName: userInfo.given_name,
          lastName: userInfo.family_name,
          email: userInfo.email,
          id: userInfo.sub
        }));
        expect(window.location.reload).toBeCalledTimes(1);
    });

    describe("and an account doesnt exist", () => {

      it("clicks the create account button", async() => {
        //Assemble
        var response: any = {status: 403, json: () =>  {}};
        fetchMock.mockImplementation((thing1: any, thing2: any) => Promise.resolve(response));

        var testClick = jest.fn();
        var button: { ref: (arg0: { click: (() => void) | (() => void); }) => void; };
        //Act
        await act(async () => {
          loginButton.props.onSuccess({code: "its a me mario"});
          button = testInstance.findByProps({className: "create-button"}).props.render({onClick: ()=>{}, disabled: false});
          button.ref({click: ()=>{testClick()}});
          testInstance.findByProps({dialogTitle: "Account Doesn't Exist"}).props.onAccept();
        });

        //Assert
        expect(fetchMock).toBeCalledTimes(1);
        expect(testClick).toBeCalledTimes(1);
      });

      it("returns to the main view", async() => {
        //Assemble
        var response: any = {status: 403, json: () =>  {}};
        fetchMock.mockImplementation((thing1: any, thing2: any) => Promise.resolve(response));

        //Act
        await act(async () => {
          loginButton.props.onSuccess({code: "its a me mario"});
        });
        await act(async () => {
          testInstance.findByProps({dialogTitle: "Account Doesn't Exist"}).props.onCancel();
        });

        //Assert
        expect(fetchMock).toBeCalledTimes(1);
        expect(testInstance.findByProps({dialogTitle: "Account Already Exists"}).props.open).toBeFalsy();
        expect(testInstance.findByProps({dialogTitle: "Account Doesn't Exist"}).props.open).toBeFalsy();
        expect(testInstance.findByProps({buttonText: "OK"}).props.open).toBeFalsy();
      });

    });

  });

  describe("when creating an account", () => {
    var createButton = testInstance.findByProps({className: 'create-button'});

    it("opens a dialog when an account already exists", async () => {
      //Assemble
      var response: any = {status: 403, json: () =>  {}};
      fetchMock.mockImplementation((thing1: any, thing2: any) => Promise.resolve(response));

      //Act
      await act(async () => {
        createButton.props.onSuccess({code: "its a me mario"});
      });

      //Assert
      expect(fetchMock).toBeCalledTimes(1);
      expect(testInstance.findByProps({dialogTitle: "Account Already Exists"}).props.open).toBeTruthy();
    });

    it("opens an error dialog when it cant connect to the backend", async() =>  {
        //Assemble
        var response: any = {status: 404, json: () =>  {}};
        fetchMock.mockImplementation((thing1: any, thing2: any) => Promise.resolve(response));
      
        //Act
        await act(async () => {
          createButton.props.onSuccess({code: "its a me mario"});
        });
      
        //Assert
        expect(fetchMock).toBeCalledTimes(1);
        var errorPopup = testInstance.findByProps({buttonText: 'OK'});
        expect(errorPopup.props.open).toBeTruthy();
        expect(errorPopup.props.dialogTitle).toEqual("Connection Error");
        expect(errorPopup.props.dialogDescription).toEqual("An unknown error occurred in communication with the API.");

        //Cleanup
        await act(async () => {
          errorPopup.props.onAccept();
        });
    });

    it("starts a user session on the frontend and redirects to the home page when successful", async () =>  {
        //Assemble
        var userInfo = {
          given_name: "hello",
          family_name: "world",
          email: "mario@world.com",
          sub: "ahhhhh"
        }
        async function callback() {
          return userInfo
        }
        var apiResponse: any = {status: 200, json: callback};
        fetchMock.mockImplementation((thing1: any, thing2: any) => Promise.resolve(apiResponse));
      
        //Act
        await act(async () => {
          createButton.props.onSuccess({code: "its a me mario"});
        });
      
        //Assert
        expect(fetchMock).toBeCalledTimes(1);
        expect(sessionMock).toBeCalledTimes(1);
        expect(sessionMock).toBeCalledWith("user", JSON.stringify({
          firstName: userInfo.given_name,
          lastName: userInfo.family_name,
          email: userInfo.email,
          id: userInfo.sub
        }));
        expect(window.location.reload).toBeCalledTimes(1);
    });

    describe("and the account already exists", () => {

      it("logs the user in and redirects to the home page", async() => {
        //Assemble
        var userInfo = {
          given_name: "hello",
          family_name: "world",
          email: "mario@world.com",
          sub: "ahhhhh"
        }
        async function callback() {
          return userInfo
        }
        var response: any = {status: 403, json: callback};
        fetchMock.mockImplementation((thing1: any, thing2: any) => Promise.resolve(response));
      
        //Act
        await act(async () => {
          createButton.props.onSuccess({code: "its a me mario"});
        });
        await act(async () => {
          testInstance.findByProps({dialogTitle: "Account Already Exists"}).props.onAccept();
        });
      
        //Assert
        expect(fetchMock).toBeCalledTimes(1);
        expect(sessionMock).toBeCalledTimes(1);
        expect(sessionMock).toBeCalledWith("user", JSON.stringify({
          firstName: userInfo.given_name,
          lastName: userInfo.family_name,
          email: userInfo.email,
          id: userInfo.sub
        }));
        expect(window.location.reload).toBeCalledTimes(1);
      });

      it("returns to the main view", async () => {
        //Assemble
        var response: any = {status: 403, json: () =>  {}};
        fetchMock.mockImplementation((thing1: any, thing2: any) => Promise.resolve(response));

        //Act
        await act(async () => {
          createButton.props.onSuccess({code: "its a me mario"});
        });
        await act(async () => {
          testInstance.findByProps({dialogTitle: "Account Already Exists"}).props.onCancel();
        });

        //Assert
        expect(fetchMock).toBeCalledTimes(1);
        expect(testInstance.findByProps({dialogTitle: "Account Already Exists"}).props.open).toBeFalsy();
        expect(testInstance.findByProps({dialogTitle: "Account Doesn't Exist"}).props.open).toBeFalsy();
        expect(testInstance.findByProps({buttonText: "OK"}).props.open).toBeFalsy();
      });

    })

  });

});

