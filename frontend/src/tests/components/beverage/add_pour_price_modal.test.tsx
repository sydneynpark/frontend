import PourPriceModal from '../../../components/add-beverage/pour_price_modal'
import TestRenderer, { ReactTestInstance, act } from 'react-test-renderer';
import React from 'react';

//create spys
var fetchMock = jest.spyOn(global, 'fetch');
fetchMock.mockImplementation((thing1: any, thing2: any) => {Promise.resolve({body: Promise.resolve({})})});
var mockOnCancel = jest.fn();
var mockOnSave = jest.fn();

function delay(ms: number) {
  return new Promise( resolve => setTimeout(resolve, ms) );
}

//initialize tested module
// var test = <PourPriceModal pourPrices={[]} open={true} onCancel={mockOnCancel} onSave={mockOnSave}></PourPriceModal>;
// console.log(test)
// const testRenderer = TestRenderer.create(
//       test
//   );
// const testInstance = testRenderer.root;

describe("Pour Price Modal", () => {

  it("renders all it's components correctly", () => {
    // //check that the table is rendered 
    // expect(testInstance.findByProps({id: "pour-price-table"})).toBeDefined();
    // //check that the input fields rendered
    // expect(testInstance.findByProps({id: "pour-price-selector"})).toBeDefined();
    // expect(testInstance.findByProps({id: "pour-size-selector"})).toBeDefined();
    // //check that the modal is open
    // expect(testInstance.findByProps({id: "pour-price-modal"}).props.open).toBeTruthy();
    // //check that the error toast isn't showing
    // expect(testInstance.findByProps({id: "pour-price-error-toast"})).toBeDefined();
  });

  // it("retrieves the pour_prices and pour_sizes", async () => {
  //   await act(async () => testInstance.findByProps({id:'add-button'}).props.onClick());

  //   expect(fetchMock).toBeCalledTimes(2);
  //   expect(fetchMock).toBeCalledWith("http://TestURL/pour_prices/prices", {method: "GET"});
  //   expect(fetchMock).toBeCalledWith("http://TestURL/pour_sizes", {method: "GET"});
  // });

  // it("saves the correct pour_prices", () => {
  //   act( () =>  testInstance.findByProps({id:'save-pour-prices-button'}).props.onClick());

  //   expect(mockOnSave).toBeCalledWith([]);
  // });

  // it("cancels on close", () => {
  //    act( () =>  testInstance.findByProps({id:'cancel-button'}).props.onClick());

  //   expect(mockOnCancel).toBeCalled();
  // });

});

