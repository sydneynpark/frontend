import AddBeverageTypeModal from '../../components/add_beverage_type_modal';
import React from 'react';
import TestRenderer, { ReactTestInstance, act } from 'react-test-renderer';
import ReactDOM from 'react-dom';
import { mount, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Enzyme from 'enzyme';


var fetchMock = jest.spyOn(global, 'fetch');
const testFunc = () => {
  //Left empty purposefully
}


Enzyme.configure({ adapter: new Adapter() })

describe("Add Beverage Type Modal", () => {

  beforeEach(() => {
    fetchMock.mockClear();
  })

  test("Missing data should result in fail", () => {
    let test_state =
    {
      type_name: "",
      description: "",
    };

    const expected_post_data_missing = new FormData();
    expected_post_data_missing.append("name", test_state.type_name);
    expected_post_data_missing.append("description", test_state.description);

    const wrapper = shallow(<AddBeverageTypeModal show={true} successfulCreation={testFunc} unsuccessfulCreation={testFunc} cancel={() => { }} />);
    wrapper.setState(test_state);
    wrapper.find('#save-button').simulate('click', { preventDefault: () => { } });
    expect(fetchMock).not.toHaveBeenLastCalledWith('http://TestURL/beverage_types/',
      { method: 'POST', body: expected_post_data_missing });
  });

  test("Valid data should not error", () => {
    let test_state =
    {
      type_name: "TestBevTypeA",
      description: "TestBevTypeDescriptionA",
    };

    const expected_post_data_valid = new FormData();
    expected_post_data_valid.append("type_name", test_state.type_name);
    expected_post_data_valid.append("description", test_state.description);

    const wrapper = shallow(<AddBeverageTypeModal show={true} successfulCreation={testFunc} unsuccessfulCreation={testFunc} cancel={() => { }} />);
    wrapper.setState(test_state);
    wrapper.find('#save-button').simulate('click', { preventDefault: () => { } });
    expect(fetchMock.mock.calls[0][0]).toBe("http://TestURL/beverage_types")
    expect(fetchMock.mock.calls[0][1]["body"].values).toBe(expected_post_data_valid.values)
  });

  test("All data is sent in request", async () => {
    //Assemble
    let test_state =
    {
      type_name: "TestBevTypeA",
      description: "TestBevTypeDescriptionA"
    };

    async function callback() {
      return test_state;
    }
    var apiResponse: any = { status: 200, json: callback };
    fetchMock.mockImplementation((thing1: any, thing2: any) => Promise.resolve(apiResponse));
    const wrapper = shallow(<AddBeverageTypeModal show={true} successfulCreation={testFunc} unsuccessfulCreation={testFunc} cancel={() => { }} />);
    wrapper.setState(test_state);

    const expected_post_data = new FormData();
    expected_post_data.append("type_name", test_state.type_name);
    expected_post_data.append("description", test_state.description);

    //Act
    fetchMock.mockClear();
    await act(async () => {
      wrapper.find('#save-button').simulate('click', { preventDefault: () => { } });
    });

    //Assert
    expect(fetchMock.mock.calls[0][0]).toBe("http://TestURL/beverage_types")
    expect(fetchMock.mock.calls[0][1]["body"].values).toBe(expected_post_data.values)
  });



});
