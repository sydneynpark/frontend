import TestRenderer, { ReactTestInstance, act } from 'react-test-renderer';
import React from 'react';
import {Redirect} from 'react-router-dom';
import MenuPage from '../../pages/menu_page'
import {MenuProps} from '../../components/menu/menu'
import mapper from '../../components/common/menuTypeMap'


//create spys
var fetchMock = jest.spyOn(global, 'fetch');
fetchMock.mockClear()

var typeMapSpy = jest.spyOn(mapper, "mapResponseToStyle")

//create fake menu
var fakeresponse = {
    menu_config: [
        {
            id: 1,
            name: "rows",
            datatype: "integer",
            default_value: "4",
            value: null
        },
        {
            id: 2,
            name: "columns",
            datatype: "integer",
            default_value: "2",
            value: null
        },
        {
            id: 3,
            name: "use_background_image",
            datatype: "boolean",
            default_value: "false",
            value: null
        },
        {
            id: 4,
            name: "background_image",
            datatype: "string",
            default_value: "nil",
            value: null
        },{
            id: 7,
            name: "title_font",
            datatype: "string",
            default_value: "stentiga",
            value: null
        },{
        
            id: 8,
            name: "text_font",
            datatype: "string",
            default_value: "sans-serif",
            value: null
        },{
        
            id: 9,
            name: "title",
            datatype: "string",
            default_value: "Test Menu",
            value: "Tested Test Menu"
        },
        {
            id: 10,
            name: "include_description",
            datatype: "boolean",
            default_value: "true",
            value: null
        },
        {
            id: 11,
            name: "include_pours",
            datatype: "boolean",
            default_value: "true",
            value: null
        },
        {
            id: 12,
            name: "include_producers",
            datatype: "boolean",
            default_value: "true",
            value: null
        },
        {
            id: 5,
            name: "background_color",
            datatype: "string",
            default_value: "#404040",
            value: null
        },
        {
            id: 6,
            name: "beverage_card_color",
            datatype: "string",
            default_value: "#989898",
            value: null
        }
    ],
    beverages: [
        {
            id: 1,
            abv: 4.5,
            name: "Beer 1",
            color: "#a66f0a",
            description: "This is Beer 1, I am not very creative",
            beverage_type: {
                id: 1,
                type_name: "IPA",
                description: "This is an IPA, it doesnt taste very good because it is an IPA"
            },
            producer: {
                id: 1,
                name: "Guy",
                description: "That one guy."
            },
            pour_prices: [
                {
                    id: 1,
                    pour_price: 3.0,
                    pour_size: {
                        id: 1,
                        size: "3oz"
                    }
                }
            ]
        }
    ]
}

//add mock for menu
jest.mock('../../components/menu/menu.tsx',  () => {
    var mockMenu: React.FC<MenuProps> = (props: MenuProps) => {
      return <div className={'menu'}></div>;
    }
    return {
      __esModule: true,
      default: mockMenu,
    }
  });

//add sleep
function delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
}

//initialize tested module
const menu = <MenuPage match={{params: {menu_id: 1}}} ></MenuPage>
 const testRenderer = TestRenderer.create(
     menu
 );
 var testInstance = testRenderer.root;


describe("MenuPage", () => {


  it("sends a fetch request", () => {
        expect(fetchMock).toBeCalledTimes(1);
  });

  describe("after sending a fetch request", () => {

    beforeEach(() =>  {
        fetchMock.mockClear();
    })
    
    it("renders an error message when it cant connect to the api", async () => {
        var apiResponse: any = {status: 500, ok: false};
        fetchMock.mockImplementation((thing1: any, thing2: any) => Promise.reject(apiResponse));

        var tempRenderer = TestRenderer.create(menu)
        var tempTestInstance = tempRenderer.root;
        await delay(100)

        expect(fetchMock).toBeCalledTimes(1);
        var errorPopup = tempTestInstance.findByProps({buttonText: 'OK'});
        expect(errorPopup.props.open).toBeTruthy();
    })

    it("renders an error message when not ok", async () => {
        var apiResponse: any = {status: 404, ok: false};
        fetchMock.mockImplementation((thing1: any, thing2: any) => Promise.resolve(apiResponse));

        var tempRenderer = TestRenderer.create(menu)
        var tempTestInstance = tempRenderer.root;
        await delay(100)

        expect(fetchMock).toBeCalledTimes(1);
        var errorPopup = tempTestInstance.findByProps({buttonText: 'OK'});
        expect(errorPopup.props.open).toBeTruthy();
    })

    it("renders a Menu when ok", async() => {
        
        async function callback() {
            return fakeresponse
        }
        var apiResponse: any = {status: 200, json: callback, ok: true};
        fetchMock.mockImplementation((thing1: any, thing2: any) => Promise.resolve(apiResponse));

        var tempRenderer = TestRenderer.create(menu)
        var tempTestInstance = tempRenderer.root;
        await delay(100)

        expect(fetchMock).toBeCalledTimes(1);
        var menuComponent = tempTestInstance.findByProps({className: 'menu'});
        expect(menuComponent).toBeTruthy()
        expect(typeMapSpy).toBeCalledTimes(1);
    })

  });

});

