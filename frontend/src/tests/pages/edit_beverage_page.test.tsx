import EditBeverageTemplate from '../../components/common/edit_beverage_template';
import NewBeveragePage from '../../pages/new_beverage';
import React from 'react';
import TestRenderer, { ReactTestInstance, act } from 'react-test-renderer';


const editBevPage = <NewBeveragePage></NewBeveragePage>;
let fetchMock = jest.spyOn(global, 'fetch');
jest.mock('../../components/add-beverage/pour_price_modal.tsx', () => {
    return {
      PourPriceModal: (props: any) => <div id={props.id || 'Dialog'}>{props.children}</div>,
    };
  })

let mockFailedResponse = Promise.resolve({
    status: 404,
    json: () => Promise.resolve({})
});

describe("Edit Beverage template", () => {

    describe("When populating the beverage type and producer dropdowns", () => {
        it("should not crash the page on a non-200 response", () => {
            
            // // Return a 404 when it asks for beverage_types or producers
            // fetchMock.mockImplementation(() => mockFailedResponse);

            // // Now build the page to see how it handles the error
            // let testRenderer = TestRenderer.create(editBevPage);
            // let testInstance = testRenderer.root;

            // // Look for key elements that indicate the page was still loaded
            // testInstance.find((child) => child.props.id == "save-button");
            // testInstance.find((child) => child.props.id == "bev-producer"); // producer should have loaded
            // testInstance.find((child) => child.props.id == "bev-type"); // beverage type should have loaded
        
        });
    });
});