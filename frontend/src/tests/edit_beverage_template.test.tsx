import EditBeverageTemplate from '../components/common/edit_beverage_template';
import React from 'react';
import TestRenderer, { ReactTestInstance, act } from 'react-test-renderer';
import ReactDOM from 'react-dom';
import { mount, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Enzyme from 'enzyme';


var fetchMock = jest.spyOn(global, 'fetch');


Enzyme.configure({ adapter: new Adapter() })

describe("Edit Bev Template", () => {

  beforeEach(() => {
    fetchMock.mockClear();
  })

  test("Missing data should result in fail", () => {
    let test_state =
    {
      name: "TestBevA",
      producer: "",
      abv: "4.2",
      color: "FF00FF",
      type: "",
      description: "This should not get added",
      beverage_types: [],
      producers: [],
      successfulRequest: false,
      showSnackbar: false,
      snackbarText: ""
    };
    const wrapper = shallow(<EditBeverageTemplate />);
    wrapper.setState(test_state);
    wrapper.find('#bev-form').simulate('submit', { preventDefault: () => { } });
    expect(wrapper.find('#bev-producer').prop('error')).toEqual(true);
    expect(wrapper.state('successfulRequest')).toEqual(false)
  });

  test("Valid data should not error", () => {
    let test_state =
    {
      name: "TestBevA",
      producer: "TestProducer",
      abv: "4.2",
      color: "FF00FF",
      type: "IPA",
      description: "This should not get added",
      beverage_types: ["IPA"],
      producers: ["TestProducer"],
      errors: {
        name: false,
        producer: false,
        abv: false,
        color: false,
        description: false
      },
      successfulRequest: false,
      showSnackbar: false,
      snackbarText: ""
    };
    const wrapper = shallow(<EditBeverageTemplate />);
    wrapper.setState(test_state);
    wrapper.find('#bev-form').simulate('submit', { preventDefault: () => { } });
    expect(wrapper.find('#bev-producer').prop('error')).toEqual(false);
    expect(wrapper.find('#bev-name').prop('error')).toEqual(false);
    expect(wrapper.find('#bev-abv').prop('error')).toEqual(false);
    expect(wrapper.find('#bev-description').prop('error')).toEqual(false);

  });

  test("All data is sent in request", async () => {
    //Assemble
    let test_state =
    {
      name: "TestBevA",
      producer: "TestProducer",
      abv: "4.2",
      color: "FF00FF",
      type: "IPA",
      description: "This should not get added",
      beverage_types: ["IPA"],
      producers: ["TestProducer"]
    };
    async function callback() {
      return test_state;
    }
    var apiResponse: any = { status: 200, json: callback };
    fetchMock.mockImplementation((thing1: any, thing2: any) => Promise.resolve(apiResponse));
    const wrapper = shallow(<EditBeverageTemplate />);
    wrapper.setState(test_state);


    const expected_post_data = new FormData();
    expected_post_data.append("abv", test_state.abv);
    expected_post_data.append("name", test_state.name);
    expected_post_data.append("color", test_state.color);
    expected_post_data.append("description", test_state.description);
    expected_post_data.append("beverage_type", test_state.type);
    expected_post_data.append("producer_id", test_state.producer);

    //Act
    fetchMock.mockClear();
    await act(async () => {
      wrapper.find('#bev-form').simulate('submit', { preventDefault: () => { } });
    });

    //Assert
    expect(fetchMock.mock.calls[0][0]).toBe("http://TestURL/beverages")
    expect(fetchMock.mock.calls[0][1]["body"].values).toBe(expected_post_data.values)
  });



});
