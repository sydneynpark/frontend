import React from 'react';
import NavBar from '../components/common/navbar';
import "../css/pages/new_beverage.css";
import EditBeverageTemplate from "../components/common/edit_beverage_template"
import '../css/pages/page.css'

class EditBeveragePage extends React.Component<any, {bevID: number}> {
    constructor(props : any) {
      super(props);

      var bevId = (this.props.match.params as any).beverage_id;
      this.state = {bevID : bevId};
    };

  render() {
    return(
    <div style={{ height: '100%', display: 'flex', flexFlow: 'column'}}>
      <NavBar />
      <div className={'page'}>
        <EditBeverageTemplate bevID={this.state.bevID}/>
      </div>
    </div>)
  }
}
export default EditBeveragePage;
