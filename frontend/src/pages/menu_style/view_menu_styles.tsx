import React from 'react';
import GlobalThemeProvider from '../../components/common/themeProvider';
import { ExpansionPanel, ExpansionPanelSummary, Typography, ExpansionPanelDetails, makeStyles, Theme, createStyles, Grid, Button, createMuiTheme, ThemeProvider, Snackbar, IconButton } from '@material-ui/core';
import { APICall, Endpoint } from '../../util/apiCall';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {isMobile} from 'react-device-detect';

import '../../css/pages/view_menu_styles.css';
import { STORAGE_NAME_TO_FONT_FAMILY } from '../../components/common/font-selector';
import { MenuConfigOption } from './edit_style_template';
import { createConfigOptionsList, createConfigValuesMap } from './map_response';
import MenuPreview from './menu_preview';

type StyleListProps = {

};

type StyleListState = {
    loading: boolean
    menuConfigs: config_with_items[],
    showSnackbar: boolean,
    snackbarMessage: string
};

const defaultState: StyleListState = {
    loading: true,
    menuConfigs: [],
    showSnackbar: false,
    snackbarMessage: ""
};


class MenuStyleList extends React.Component<StyleListProps, StyleListState> {
    constructor(props: StyleListProps) {
        super(props);
        this.state = defaultState;

        this.deleteConfig = this.deleteConfig.bind(this);

        this.fetchMenuStylings();
    }

    private fetchMenuStylings() {
        APICall.to(Endpoint.MenuConfigs, 'with_values').get()
        .then(response => response.json())
        .then(data => {
            let parsed_configs = data as config_with_items[];

            this.setState({
                loading: false,
                menuConfigs: parsed_configs
            });
        })
        .catch(err => {
            this.setState({
                showSnackbar: true,
                snackbarMessage: "There was an error fetching menu styles."
            });
        });
    }

    private deleteConfig(event: React.MouseEvent<HTMLButtonElement, MouseEvent>) {
        event.preventDefault();

        let configId: number = Number(event.currentTarget.id);

        let success = !isNaN(configId);

        if (success) {
            APICall.to(Endpoint.MenuConfigs).delete(configId)
            .then((response) => {

                let indexToRemove = this.state.menuConfigs.findIndex(config => config.menu_config_data.id == configId);
                let nameOfTheme = this.state.menuConfigs[indexToRemove].menu_config_data.name;

                this.state.menuConfigs.splice(indexToRemove, 1);
                
                this.setState({
                    menuConfigs: this.state.menuConfigs,
                    showSnackbar: true,
                    snackbarMessage: `Successfully deleted menu style: ${nameOfTheme}`
                })
            }).catch(err => { success = false; });
        }

        if (!success) {
            this.setState({
                showSnackbar: true,
                snackbarMessage: "There was an error deleting the menu style."
            });
        }
    }

    private samplePreview(configOptionsList: config_item[]): JSX.Element {
        let configOptionsProp: MenuConfigOption[] = createConfigOptionsList(configOptionsList);
        let configValuesProp: Map<string, any> = createConfigValuesMap(configOptionsList);

        return <MenuPreview configOptions={configOptionsProp} configValues={configValuesProp} scaleFactor={0.3}/>
    }

    private createMenuConfigCard(menuConfigInfo: config_with_items): JSX.Element {
        return <ExpansionPanel>

            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon/>} aria-controls="panel1bh-content" id="panel1bh-header">
                <Grid container direction='row'>
                    <Grid item xs={4}>
                        <Typography className={"configTitle"}>
                            {menuConfigInfo.menu_config_data.name}
                        </Typography>
                    </Grid>
                    <Grid item xs={8}>
                        <Typography >
                            {menuConfigInfo.menu_config_data.description}
                        </Typography>
                    </Grid>
                </Grid>

            </ExpansionPanelSummary>

            <ExpansionPanelDetails>
                <Grid container direction='column'>
                    { isMobile ||
                        <Grid container direction='row'>
                            {this.samplePreview(menuConfigInfo.menu_config_options)}
                        </Grid>
                    }
                    <Grid container direction='row' alignContent='center' justify='flex-end'>
                        <Button className={'actionButton'} variant='outlined' color='secondary' 
                            style={{margin: '10px'}}
                            key={`${menuConfigInfo.menu_config_data.id}`} 
                            id={`${menuConfigInfo.menu_config_data.id}`} 
                            onClick={this.deleteConfig}
                        >
                            Delete
                        </Button>
                        <Button className={'actionButton'} variant='contained' color='primary' style={{margin: '10px'}} href={`/editMenuStyle/${menuConfigInfo.menu_config_data.id}`}>Edit</Button>
                    </Grid>
                </Grid>
            </ExpansionPanelDetails>

        </ExpansionPanel>
    }

    render() {
        return <GlobalThemeProvider>
            <div className={"expansionPanelSummary"}>
                {this.state.menuConfigs.map(menuConfig => this.createMenuConfigCard(menuConfig))}
            </div>
            <Snackbar
                open={this.state.showSnackbar}
                autoHideDuration={6000}
                onClose={(e) => this.setState({ showSnackbar: false })}
                message={this.state.snackbarMessage}
                action={
                <React.Fragment>
                    <IconButton size="small" aria-label="close" color="inherit" onClick={(e) => this.setState({ showSnackbar: false })}>
                    </IconButton>
                </React.Fragment>
                }
            />

        </GlobalThemeProvider>
    }
}

export default MenuStyleList;