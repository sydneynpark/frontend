import React from 'react';
import NavBar from '../../components/common/navbar';
import MenuStyleList from './view_menu_styles';
import { Grid, Button } from '@material-ui/core';
import GlobalThemeProvider from '../../components/common/themeProvider';

const ViewMenuStylesPage: React.FC = () => {
    return <GlobalThemeProvider><div>
        <NavBar/>
        <br/>
        <Grid container direction='column' alignItems='center' alignContent='space-around'>
            <Grid item>
                <h2>Your Menu Styles</h2>
            </Grid>
            <Grid item>
                <Button href='/menuStyle' variant='contained' color='primary'>
                    Create New Style
                </Button>
            </Grid>
        </Grid>

        <br/>

        <div className={'page'}>
            <MenuStyleList/>
        </div>
    </div></GlobalThemeProvider>;
}

export default ViewMenuStylesPage;