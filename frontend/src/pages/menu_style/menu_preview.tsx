
import React from 'react';
import { MenuConfigOption } from './edit_style_template';
import Menu from '../../components/menu/menu';
import { APICall, Endpoint } from '../../util/apiCall';

type MenuPreviewState = {
    sampleBeers: beverage[];
    loading: boolean;
    scaleFactor: number;
}

const SAMPLE_BEER: beverage = {
    id: 123,
    abv: 6.0,
    name: "Hop Hop Boom",
    color: "#BF923B",
    description: "Hoppy, tropical and citrus fruit forward, hazy pale ale.",
    beverage_type: {
        id: 1,
        type_name: "IPA",
        description: "India Pale Ale"
    },
    beverage_type_id: 1,
    pour_prices: [
        {
            pour_price: 5,
            pour_size_id: 1,
            pour_size: {
                id: 1,
                size: "4oz"
            }
        },
        {
            pour_price: 8,
            pour_size_id: 1,
            pour_size: {
                id: 1,
                size: "12oz"
            }
        },
        {
            pour_price: 15,
            pour_size_id: 1,
            pour_size: {
                id: 1,
                size: "Growler"
            }
        }
    ],
    producer: {
        id: 1,
        name: "R'Noggin Brewing Co.",
        description: "Two brothers put their noggins together and decided to craft great beer."
    }
}

type MenuPreviewProps = {
    configOptions: MenuConfigOption[];
    configValues: Map<string, any>;
    scaleFactor?: number;
}

export class MenuPreview extends React.Component<MenuPreviewProps, MenuPreviewState> {
    
    constructor(props: MenuPreviewProps) {

        super(props);

        let longBeersList = new Array(5).fill(SAMPLE_BEER);
        this.state = {
            sampleBeers: longBeersList,
            // sampleBeers: [],
            loading: true,
            scaleFactor: this.props.scaleFactor? this.props.scaleFactor : 1
        }

        APICall.to(Endpoint.Beverages)
        .get()
        .then(response => response.json())
        .then(data => {
            let beverageList = data as beverage[];
            this.setState({
                // sampleBeers: beverageList,
                // TODO fix bug where menu can't display if not enough beers
                loading: false
            });
        });
    }

    private buildStyle(): menu_style_options {
        let styleValueDict = this.props.configValues;
        let styleOptionList = this.props.configOptions;

        let style: menu_style_options = {
            display_type: (getValue("display_type") as display_type),
            table_header_color: (getValue("table_header_color") as string),
            rows: (getValue("rows") as number),
            columns: (getValue("columns") as number),
            use_background_image: (getValue("use_background_image") as boolean),
            background_image: (getValue("background_image") as string),
            background_color: (getValue("background_color") as string),
            beverage_card_color: (getValue("beverage_card_color") as string),
            title_font: (getValue("title_font") as string),
            text_font: (getValue("text_font") as string),
            title: (getValue("title") as string),
            include_description: (getValue("include_description") as boolean),
            include_producers: (getValue("include_producers") as boolean),
            include_pours: (getValue("include_pours") as boolean)
        }

        function getValue(optionName: string) {
            let found = styleValueDict.get(optionName);

            if (found === undefined || found===null) {
                found = styleOptionList.find((option) => option.name == optionName)?.default_value;
            } else {
            }
            return found;

        }

        return style;
    }

    render() { 
        return <React.Fragment>
            { !this.state.loading && 
            // TODO - these divs are my attempt to lock the aspect ratio of the menu preview
            // This solution doesn't seem to be working but I wanted to leave it in so we
            //  have something to build off of
                <div className={"container"} style={{
                    height: '0',
                    width: '100%',
                    paddingTop: '56%',
                    overflow: 'hidden',
                    position: 'relative'
                }}>
                    <div className={"container"} style={{
                        position: 'absolute',
                        top: '0',
                        left: '0',
                        width: '100%',
                        height: '100%'
                    }}>
                        <Menu style={this.buildStyle()} beers={this.state.sampleBeers} scaling={this.state.scaleFactor}/>
                    </div>
                </div>
            }
        </React.Fragment>
    }
}

export default MenuPreview;