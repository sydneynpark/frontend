import React from 'react';
import * as InputHelpers from '../../components/common/inputFields';
import Grid from "@material-ui/core/Grid"
import { TextField, Button, ThemeProvider, createMuiTheme, FormControl, Snackbar, IconButton, Dialog, DialogContent, DialogActions } from '@material-ui/core';
import Menu from '../../components/menu/menu';
import { APICall, Endpoint } from '../../util/apiCall';
import { MenuPreview } from './menu_preview';
import '../../css/components/edit_beverage_template.css';
import Error404 from '../../components/common/error404';
import { createConfigOptionsList, createConfigValuesMap } from './map_response';

export class MenuConfigOption {
    id: number;
    name: string;
    prettified_name: string;
    datatype: string;
    default_value: string;

    prettify(ugly: string, datatype: string): string {
        let pretty = ugly.split("_").join(" "); // TS has no replaceAll
        switch (datatype) {
            case "integer":
                pretty = "Number of " + pretty + ":";
                break;
            case "boolean":
                pretty += "?";
                break;
            default:
                pretty += ":";
                break;
        }
        pretty = pretty.charAt(0).toUpperCase() + pretty.slice(1);
        return pretty;
    }

    constructor(data: any) {
        this.id = data.id;
        this.name = data.name;
        this.datatype = data.datatype;
        this.default_value = data.default_value;
        this.prettified_name = this.prettify(this.name, this.datatype);
    }
}

const defaultOptionsDisplayOrder: string[] = [
    "display_type",

    "title",
    "title_font",
    "text_font",

    "rows",
    "columns",

    "include_description",
    "include_pours",
    "include_producers",

    "background_color",
    "beverage_card_color",
    "table_header_color",
    "use_background_image",
    "background_image",
]

const defaultOptions: string[] = [
    "display_type",
    "title",
    "title_font",
    "text_font",
    "include_description",
    "include_pours",
    "include_producers",
    "background_color",
    "use_background_image",
    "background_image",
]

const cardOptions: string[] = [
    "rows",
    "columns",
    "beverage_card_color",
]

const gridOptions: string[] = [
    "table_header_color",
]

type MenuStylePageState = {
    name: string;
    description: string;
    allConfigOptions: MenuConfigOption[];
    configOptions: MenuConfigOption[];
    configValues: Map<string, any>;
    showPreview: boolean;
    showSnackbar: boolean;
    snackbarText: string;
    errors: Map<string, boolean>;
    errorMessages: Map<string, string>;
    show404: boolean;
}

type MenuStylePageProps = {
    styleId?: number;
    snackbarOnInit?: string;
}

const defaultState: MenuStylePageState = {
    name: "",
    description: "",
    allConfigOptions: [],
    configOptions: [],
    configValues: new Map<string, any>(),
    showPreview: false,
    showSnackbar: false,
    snackbarText: "",
    errors: new Map<string, boolean>(),
    errorMessages: new Map<string, string>(),
    show404: false
}

export class EditMenuStyleTemplate extends React.Component<MenuStylePageProps, MenuStylePageState> {

    constructor(props: MenuStylePageProps) {
        super(props);
        this.state = defaultState;

        if (this.props.styleId === undefined) {
            this.getMenuConfigOptions();
        } else {
            this.getMenuConfigOptionsFor(this.props.styleId);
        }

        this.handleNameChange = this.handleNameChange.bind(this);
        this.submitForm = this.submitForm.bind(this);
        this.handleOptionChange = this.handleOptionChange.bind(this);
        this.handleOptionColorChange = this.handleOptionColorChange.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handleOptionFontChanged = this.handleOptionFontChanged.bind(this);
        this.handleOptionBooleanChanged = this.handleOptionBooleanChanged.bind(this);
        this.handleOptionDisplayTypeChanged = this.handleOptionDisplayTypeChanged.bind(this);
    }

    /**
     * Calls to the API for all of the menu option configs that can be set on a menu styling.
     * Sets up the state to handle values for those options.
     */
    private getMenuConfigOptions() {
        APICall.to(Endpoint.MenuConfigOptions).get()
            .then(response => response.json())
            .then(data => {
                let allStylingOptions: MenuConfigOption[] = data.map((responseObject: any) => new MenuConfigOption(responseObject));
                let display_type: MenuConfigOption = allStylingOptions.find(option => option.name === "display_type") as MenuConfigOption;

                let stylingValues: Map<string, any> = this.setEmptyValues(allStylingOptions);
                stylingValues.set("display_type", display_type.default_value);

                this.setupPageState(allStylingOptions, stylingValues, display_type);
            })
            .catch(err => { this.showSnackbar("Failed to load menu configuration options. Please refresh and try again."); console.log(err); });

    }

    /**
 * Calls to the API for all of the menu option configs that can be set on a menu styling, 
 * and their values for a specific menu styling id. Sets up the state to handle values for 
 * those options and be able to update.
 * @param styleId The id of the MenuConfig we are interested in editing
 */
    private getMenuConfigOptionsFor(styleId: number) {
        APICall.to(Endpoint.MenuConfigs, 'with_values').getById(styleId)
            .then(response => response.json())
            .then(data => {

                let responseMenuConfigOptions: config_item[] = data.menu_config_options as config_item[];

                // Turn the list of menu config option/values into option definition objects
                let configOptions: MenuConfigOption[] = createConfigOptionsList(responseMenuConfigOptions);

                // Use the values and default values to populate the configValues portion of the state
                let configValues: Map<string, any> = createConfigValuesMap(responseMenuConfigOptions);

                let display_type: MenuConfigOption = configOptions.find(option => option.name === "display_type") as MenuConfigOption;

                // Use the options and values we've found and finish setting the rest of the state
                this.setupPageState(configOptions, configValues, display_type,
                    {
                        name: data.menu_config_data.name,
                        description: data.menu_config_data.description
                    });

            }).catch(err => {
                this.setState({
                    show404: true
                });
            });
    }


    private setupPageState(stylingOptions: MenuConfigOption[], stylingValues: Map<string, any>, display_type: MenuConfigOption, moreState: any = {}) {

        // Order the list of menu style options based on how we want them to display
        var orderedStylingOptions: MenuConfigOption[] = [];
        defaultOptionsDisplayOrder.forEach((option: string) => {
            let foundOption = stylingOptions.find((item) => item.name == option);
            if (foundOption) {
                orderedStylingOptions.push(foundOption);
            }
        });

        // Add any that we may have missed to the ordered list
        stylingOptions.forEach((option) => {
            if (!orderedStylingOptions.includes(option)) {
                orderedStylingOptions.push(option);
            }
        });

        // Set all the error states to false until we trigger form validation
        var errorStates = new Map<string, boolean>([
            ["name", false],
            ["description", false]
        ]);
        orderedStylingOptions.forEach((option) => {
            errorStates.set(option.name, false);
        });

        // Set all the error messages to empty until we trigger form validation
        var errorMessages = new Map<string, string>([
            ["name", ""],
            ["description", ""]
        ]);
        orderedStylingOptions.forEach((option) => {
            errorMessages.set(option.name, "");
        });

        let orderedFilteredStylingOptions = this.filterOptionsForDisplayType(display_type.default_value as display_type, orderedStylingOptions);

        this.setState({
            ...moreState,
            allConfigOptions: orderedStylingOptions,
            configOptions: orderedFilteredStylingOptions,
            configValues: stylingValues,
            errors: errorStates,
            errorMessages: errorMessages
        });

    }



    /**
     * Filters the options based on the display type
     * @param stylingOptions 
     */
    private filterOptionsForDisplayType(display_type: display_type, stylingOptions: MenuConfigOption[]): MenuConfigOption[] {
        let filteredOptions: MenuConfigOption[] = [];

        //get the options for the display type
        let desiredOptions: String[] = defaultOptions;
        if (display_type === "grid") {
            desiredOptions = desiredOptions.concat(gridOptions);
        } else if (display_type === "card") {
            desiredOptions = desiredOptions.concat(cardOptions);
        } else {
            //todo idk
        }

        //filter the existing options
        stylingOptions.forEach(option => {
            if (desiredOptions.includes(option.name.trim())) {
                filteredOptions.push(option);
            }
        });

        return filteredOptions;
    }

    /**
     * Popup the snackbar to show when an error occurred in communication with the API.
     * @param errorMessage 
     */
    private showSnackbar(errorMessage: string) {
        this.setState({
            snackbarText: errorMessage,
            showSnackbar: true
        });
    }

    //#region Callbacks for value bindings

    /**
     * Called when the name of the menu styling is changed.
     * @param e source event generated by the Name input field
     */
    private handleNameChange(e: any) {
        const name = (e.target as HTMLInputElement).value;

        let newState: any = {
            name: name
        }

        let needsValidation = this.state.errors.get("name") == true;
        if (needsValidation) {
            let nameValid = this.validateOptionValue("name", name);
            this.state.errors.set("name", !nameValid);
            newState.errors = this.state.errors;
        }

        this.setState(newState);
    }

    /**
     * Called when the description of the menu styling is changed.
     * @param e event generated by the Description input field
     */
    private handleDescriptionChange(e: any) {
        let description = (e.target as HTMLInputElement).value;

        let newState: any = {
            description: description
        };

        let needsValidation = this.state.errors.get("description") == true;
        if (needsValidation) {
            let descriptionValid = this.validateOptionValue("description", description);
            this.state.errors.set("description", !descriptionValid);
            newState.errors = this.state.errors;
        }

        this.setState(newState);
    }

    /**
     * Handles when text fields are updated. Works for both text and number fields.
     * @param e The event sent by the input field
     */
    private handleOptionChange(e: any) {
        let inputElement = (e.target as HTMLInputElement);
        this.saveChangedOption(inputElement.id, inputElement.value);
    }

    /**
     * TODO
     * @param sourceId 
     * @param newColor 
     */
    private handleOptionDisplayTypeChanged(sourceId: string, newDisplayType: display_type | null) {
        let prevValue: display_type = this.state.configValues.get("display_type")

        this.saveChangedOption(sourceId, newDisplayType);

        if (prevValue != this.state.configValues.get("display_type")) {
            let orderedStylingoptions: MenuConfigOption[] = this.filterOptionsForDisplayType(this.state.configValues.get("display_type"), this.state.allConfigOptions);

            this.setState({
                configOptions: orderedStylingoptions,
            });
        }
    }

    /**
     * Updates an option that corresponds to a color.
     * @param sourceId The ID of the color-picker, which is the option's name.
     * @param newColor The hex code of the new color that was selected.
     */
    private handleOptionColorChange(sourceId: string, newColor: string) {
        this.saveChangedOption(sourceId, newColor);
    }

    /**
     * Updates a boolean option value.
     * @param sourceId The ID of the input field, which is also the option name.
     * @param newValue The new boolean value (or null if they picked the "-" option)
     */
    private handleOptionBooleanChanged(sourceId: string, newValue: boolean | null) {
        this.saveChangedOption(sourceId, newValue);
    }

    /**
     * Updates when a font selector option was changed
     * @param e The event sent by the selector dropdown
     * @param child This param is not used but onChange signature requires it.
     */
    private handleOptionFontChanged(e: React.ChangeEvent<{
        name?: string | undefined;
        value: unknown;
    }>, child: React.ReactNode) {
        let inputElement = (e.target as HTMLInputElement);
        this.saveChangedOption(inputElement.name, inputElement.value);
    }

    /**
     * Called by any of the change handles.
     * Updates an option with the new value.
     * @param optionId Name of the option being updated
     * @param newValue Value of the option being updated
     */
    private saveChangedOption(optionId: string, newValue: any) {
        this.state.configValues.set(optionId, newValue);

        let newState: any = {
            configValues: this.state.configValues
        };

        if (this.state.errors.get(optionId)) {
            let newValidState = !this.validateOptionValue(optionId, newValue);
            this.state.errors.set(optionId, newValidState);
            newState.errors = this.state.errors;
        }

        this.setState(newState);
    }

    //#endregion Callbacks for value bindings

    //#region Form validation


    /**
     * Some settings should be validated in special ways (for instance, background_image should
     * only be required when use_background_image is true).
     * Those special validation callbacks are defined here.
     */
    private formValidationExceptions: Map<string, (optionValue: any) => boolean> = new Map([
        ["background_image",
            (backgroundImageValue: any) => {
                let requireBackgroundImage = this.state.configValues.get("use_background_image") === true;
                let valid = true;

                if (requireBackgroundImage) {
                    valid = !(backgroundImageValue === "" || backgroundImageValue === undefined || backgroundImageValue == null);
                }

                return valid;
            }
        ]
    ]);

    /**
     * Perform validation on all elements in the form.
     * 
     * First checks that the menu style has a name and a description.
     * Then runs through and validates each of the options for that style.
     */
    private validateForm() {

        let valid = true;


        this.state.configOptions.forEach(option => {
            valid = this.validateOptionValue(option.name, this.state.configValues.get(option.name)) && valid;
        });

        let nameValid = this.validateOptionValue("name", this.state.name);
        this.state.errors.set("name", !nameValid);
        let descriptionValid = this.validateOptionValue("description", this.state.description);
        this.state.errors.set("description", !descriptionValid);

        this.setState({
            errors: this.state.errors
        });

        return valid && nameValid && descriptionValid;
    }

    /**
     * Performs validation on a single styling option value.
     * @param optionName The name of the option that is being validated
     * @param value The value that is set for that option
     * @param resetState Whether to update the error state (this will give visual cues - red for error)
     */
    private validateOptionValue(optionName: string, value?: any, resetState: boolean = false): boolean {

        var optionValue = value === undefined ? this.state.configValues.get(optionName) || null
            : value;

        let valid = true;
        let validationFunction = this.formValidationExceptions.get(optionName);

        // When this specific attribute has a special way to be validated
        if (validationFunction !== undefined) {
            valid = validationFunction(value);

            // Otherwise use the standard validation - no null or empty string
        } else if (optionValue == null || optionValue === "") {
            valid = false;

        }

        if (valid) {
            this.state.errors.set(optionName, false);
            this.state.errorMessages.set(optionName, "");
        } else {
            this.state.errors.set(optionName, true);
            this.state.errorMessages.set(optionName, "This field is required.");
        }

        if (resetState) {
            this.setState({
                errors: this.state.errors,
                errorMessages: this.state.errorMessages
            });
        }

        return valid;
    }

    /**
     * Validate the form, then send a POST request if it is valid.
     * If it is not valid, don't send a POST request and display the error messages.
     * @param event The Click event on the form button. We prevent default to stop the page from refreshing.
     */
    private submitForm(event: React.SyntheticEvent) {
        event.preventDefault();

        if (this.validateForm()) {

            // Turn the options & their values into a JSON-able dictionary
            let allOptionValues: any[] = [];
            this.state.configOptions.forEach((option) => {
                allOptionValues.push({
                    menu_config_option_id: option.id,
                    value: this.state.configValues.get(option.name)
                })
            });

            // Create form data with the relevant information
            var data = new FormData();
            data.append("name", this.state.name);
            data.append("description", this.state.description);
            data.append("values", JSON.stringify(allOptionValues));

            // Send a POST or PATCH request
            let apiCall = APICall.to(Endpoint.MenuConfigs);
            let httpResponsePromise: Promise<Response> = this.props.styleId === undefined
                ? apiCall.post(data)
                : apiCall.put(this.props.styleId, data);

            httpResponsePromise
                .then(response => {
                    this.showSnackbar("Menu style was successfully saved.");
                    window.location.pathname = '/menuStyles';
                })
                .catch(err => {
                    this.showSnackbar("Error saving menu style.");
                });
        }
    }

    private setEmptyValues(stylingOptions: MenuConfigOption[]): Map<string, any> {
        let stylingValues: Map<string, any> = new Map<string, any>();

        stylingOptions.forEach((option) => {
            stylingValues.set(option.name, null);
        });

        return stylingValues;
    }

    //#endregion Form validation

    //#region Building the page

    /**
     * Builds all of the input fields that correspond to the options that can be set on a menu.
     */
    private buildInputForm() {
        let inputElements: JSX.Element[] = [];

        if (!this.state || !this.state.configOptions) {
            return null;
        }

        this.state.configOptions.forEach((option: MenuConfigOption) => {

            inputElements.push(this.buildInputFormElement(option));
        });

        return inputElements;
    }

    /**
     * Builds a single input field corresponding to one option.
     * @param option The option that is being set in the input field.
     */
    private buildInputFormElement(option: MenuConfigOption): JSX.Element {

        let input: JSX.Element;

        let errorMessage = this.state.errorMessages.get(option.name) || "";
        let errorState = this.state.errors.get(option.name) || false;
        let value = this.state.configValues.get(option.name);

        switch (option.datatype) {

            case "integer":
                input = InputHelpers.buildNumberInput(
                    option.name,
                    this.handleOptionChange,
                    errorState,
                    errorMessage,
                    value
                );
                break;

            case "boolean":
                input = InputHelpers.buildYesNoDropdown(
                    option.name,
                    this.handleOptionBooleanChanged,
                    errorState,
                    errorMessage,
                    value
                );
                break;

            case "color":
                input = InputHelpers.buildColorPicker(
                    option.name,
                    this.handleOptionColorChange,
                    errorState,
                    errorMessage,
                    value
                );
                break;

            case "font":
                input = InputHelpers.buildFontSelector(
                    option.name,
                    this.handleOptionFontChanged,
                    errorState,
                    errorMessage,
                    value
                );
                break;

            case "display_type":
                input = InputHelpers.buildDisplayTypeDropdown(
                    option.name,
                    this.handleOptionDisplayTypeChanged,
                    errorState,
                    errorMessage,
                    this.state.configValues.get(option.name) as display_type
                );
                break;

            default:
                input = InputHelpers.buildTextInput(
                    option.name,
                    this.handleOptionChange,
                    errorState,
                    errorMessage,
                    value
                );
                break;
        }

        return <Grid container item direction="row" key={option.name} xs={12} justify="space-between" alignItems="center">
            <Grid container item key="menu-config-option" xs={6}>
                <h3 style={{ width: "100%", marginLeft: 0 }}>{option.prettified_name}</h3>
            </Grid>
            <Grid container item key="menu-config-value" xs={6} justify="flex-end" alignItems="center">
                <span className={'input-field'} style={{ width: "100%" }}>{input}</span>
            </Grid>
        </Grid>

    }

    //#endregion Building the page


    render() {
        let theme = createMuiTheme({
            palette: {
                primary: {
                    main: "#008000"
                }
            }
        })

        if (this.state.show404) {
            return <Error404 />
        }

        return <ThemeProvider theme={theme}>
            <form id="style_form" onSubmit={this.submitForm}>
                <Grid container direction="column" justify="center" alignItems="flex-start" spacing={1}>
                    <Grid container direction="row" justify="center" alignItems="center">
                        <Grid item>
                            <h2 style={{ width: "100%" }}>{this.props.styleId === undefined ? "Create" : "Edit"} menu style:</h2>
                        </Grid>
                        <Grid item>
                            <TextField
                                value={this.state.name}
                                label="Menu Style Name"
                                onChange={this.handleNameChange}
                                error={this.state.errors.get("name") || false}
                                helperText={this.state.errorMessages.get("name") || ""}
                            />
                        </Grid>
                    </Grid>

                    <br />

                    <Grid container direction="row" justify="center" alignItems="center">
                        <TextField
                            fullWidth
                            value={this.state.description}
                            onChange={this.handleDescriptionChange}
                            error={this.state.errors.get("description") || false}
                            helperText={this.state.errorMessages.get("description") || ""}
                            id="description"
                            label="Description"
                            variant="outlined"
                            placeholder="Enter menu style description here"
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                    </Grid>

                    <br />

                    {this.buildInputForm()}

                    <Grid container direction="row" justify="flex-end" alignItems="center">
                        <Button variant="outlined" href="/menuStyles" style={{ margin: "10px" }}>
                            Cancel
                            </Button>
                        <Button variant="contained" color="primary" type="submit" style={{ margin: "10px" }}>
                            Save
                            </Button>
                    </Grid>

                </Grid>
            </form>

            <Snackbar
                open={this.state.showSnackbar}
                autoHideDuration={6000}
                onClose={(e) => this.setState({ showSnackbar: false })}
                message={this.state.snackbarText}
                action={<React.Fragment>
                    <IconButton size="small" aria-label="close" color="inherit" onClick={(e) => this.setState({ showSnackbar: false })}>
                    </IconButton>
                </React.Fragment>}
            />

            {/* 
            TODO - this is a popup to show the preview of the menu as you are changing the styling.
            I removed the button to show it because the menu preview style was becoming a time sink.
            But now all we have to do is add a button that sets { showPreview: true } and
            the popup will be functional again.
        */}
            <Dialog fullScreen open={this.state.showPreview} style={{ boxShadow: "none !important" }}>
                <DialogContent>
                    <MenuPreview
                        configOptions={this.state.configOptions}
                        configValues={this.state.configValues}>
                    </MenuPreview>
                </DialogContent>
                <DialogActions>
                    <Button
                        variant="contained" color="primary"
                        onClick={() => this.setState({ showPreview: false })}>
                        Close Preview
                </Button>
                </DialogActions>
            </Dialog>
        </ThemeProvider>
    }
}

export default EditMenuStyleTemplate;