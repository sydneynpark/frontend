import { MenuConfigOption } from './edit_style_template';
import { typeCastConfig } from '../../components/common/menuTypeMap';


export function createConfigOptionsList(menuConfigOptionsList: config_item[]): MenuConfigOption[] {
    return menuConfigOptionsList.map((item: config_item) => new MenuConfigOption(item));
}

export function createConfigValuesMap(menuConfigOptionsList: config_item[]): Map<string, any> {
    let configValuesMap: Map<string, any> = new Map<string, any>();
    menuConfigOptionsList.forEach( (optionValuePair: config_item) => {
        configValuesMap.set(optionValuePair.name, typeCastConfig(optionValuePair));
    });

    return configValuesMap;
}