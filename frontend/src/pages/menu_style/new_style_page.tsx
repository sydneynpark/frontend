import React from 'react';
import EditMenuStyleTemplate from './edit_style_template';
import NavBar from '../../components/common/navbar';
import '../../css/pages/page.css';

export class CreateMenuStylePage extends React.Component {
    render() {
        return <div>
            <NavBar/>
            <div className={'page'}>
                <EditMenuStyleTemplate/>
            </div>
        </div>;
    }
};

export default CreateMenuStylePage;