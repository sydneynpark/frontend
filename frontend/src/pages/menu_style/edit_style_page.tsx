import React from 'react';
import EditMenuStyleTemplate from './edit_style_template';
import NavBar from '../../components/common/navbar';
import '../../css/pages/page.css';
import { useParams } from 'react-router-dom';
import Error404 from '../../components/common/error404';

const EditMenuStylePage: React.FC = (props) => {
    let {style_id} = useParams();
    console.log(style_id);
    let numberId = Number(style_id);
    let show404 = isNaN(numberId);

    return <div>
        <NavBar/>
        { 
            (show404 && <Error404/>)
            ||
            (<div className={'page'}>
                <EditMenuStyleTemplate styleId={numberId}/>
            </div>)
        }   
    </div>;
}


export default EditMenuStylePage;