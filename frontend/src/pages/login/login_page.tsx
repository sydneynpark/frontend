import React from 'react'
import UserSession from '../../components/sessions/userSession'
import LoginCard from '../../components/login/login_card'
import '../../css/pages/login/login_page.css'
import {isBrowser} from 'react-device-detect';
import {useHistory} from 'react-router-dom'

class LoginPage extends React.Component {
  createBubbles = () => {
    let bubbles = []
    for (let i = 0; i < 7000;) {
      i = i + (200 + (Math.random() * 200));
      let temp = i + 'ms';
      bubbles.push(<div className={'bubble'} style={{animationDelay: temp}}/>)
    }
    return bubbles
  }

  render() {
    return (
        <div className={'login-page'}> 
            <div className={'login-card-container'}>
              <LoginCard></LoginCard>
            </div>
            <div className={'background-image'} >
              {this.createBubbles()}
            </div>
        </div>
    )
  }
}

export default LoginPage