import React from 'react';
import NavBar from '../components/common/navbar';
import "../css/pages/new_beverage.css";
import EditBeverageTemplate from "../components/common/edit_beverage_template"
import '../css/pages/page.css'

class NewBeveragePage extends React.Component {
  render() {
    return(
    <div style={{height: '100%', display: 'flex', flexFlow: 'column'}}>
      <NavBar />
      <div className={'page'}>
        <EditBeverageTemplate bevID={-1}/>
      </div>
    </div>)
  }
}
export default NewBeveragePage;
