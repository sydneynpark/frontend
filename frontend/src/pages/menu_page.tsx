import React from 'react';
import Menu from '../components/menu/menu'
import mapper from '../components/common/menuTypeMap'
import { Fade, CircularProgress } from '@material-ui/core'
import Alert from '../components/common/alert'
import { APICall, Endpoint } from '../util/apiCall';

type MenuPageState = {
    loading: boolean,
    style?: menu_style_options,
    beverages?: beverage[]
    error: boolean,
    errorTitle: string,
    errorMessage: string
}

class MenuPage extends React.Component<any, MenuPageState> {

    constructor(props: any) {
        super(props);
        this.state = { loading: true, error: false, errorTitle: '', errorMessage: '' }
        this.fetchMenu();
    }

    fetchMenu(): void {
        var menuId = (this.props.match.params as any).menu_id

        if (menuId <= 0) {
            this.state = {loading:true, error: true, errorTitle: "Invalid MenuID", errorMessage: "MenuID must be greater than 0"};
        } else {
            let options: RequestInit = {
                mode: 'cors',
                cache: 'default',
            };

            APICall.to(Endpoint.Menus).withOptions(options).getById(menuId)
            .then(async response => {

                var body = await response.json();

                var menuStyle: menu_style_options = mapper.mapResponseToStyle(body.menu_config as config_item[])
                var beverages: beverage[] = body.beverages as beverage[];

                this.setState({ loading: false, style: menuStyle, beverages: beverages })
            }).catch(error => {
                this.setState({error: true, errorTitle: "Error Connecting to API", errorMessage: "There was an error connecting to the API. Try reloading the page or contact the AHOPS team for help."});
            });
        }
    }

    render() {
        return (
            <React.Fragment>
                <Alert
                    dialogTitle={this.state.errorTitle}
                    dialogDescription={this.state.errorMessage}
                    buttonText={"OK"}
                    open={this.state.error}
                    onAccept={() => {
                        this.setState({error: false})
                    }}
                />
                {!this.state.loading &&
                    <Menu
                        style={this.state.style as menu_style_options}
                        beers={this.state.beverages as beverage[]}
                    ></Menu>}
            </React.Fragment>)
    }
}
export default MenuPage;