import React from 'react';
import NavBar from '../../components/common/navbar';
import { APICall, Endpoint } from '../../util/apiCall';
import { Button, Snackbar } from '@material-ui/core';
import { Grid } from '@material-ui/core';
import GlobalThemeProvider from '../../components/common/themeProvider';
import { GetAppRounded } from '@material-ui/icons';

type BackupPageProps = {}

type BackupPageState = {
    showSnackbar: boolean;
    snackbarText: string;
}

const defaultState: BackupPageState = {
    showSnackbar: false,
    snackbarText: ""
}

export class BackupPage extends React.Component<BackupPageProps, BackupPageState> {

    constructor(props: any) {
        super(props);

        this.state = defaultState;
    }

    render() {
        return <div>
            <NavBar />
            <br />
            <div className={'page'}>
                <GlobalThemeProvider>
                    <Grid container direction="column">
                        <Grid container direction="row" alignContent='center' alignItems='center'>
                            <Grid container direction="column" alignContent='center' alignItems='center'>
                                <Grid item>
                                    <h2>Download Your Data</h2>
                                </Grid>
                                <Grid item>
                                    <p style={{ margin: '10px' }}>The downloaded file will contain a record of all data that is stored in the database right now.
                                        In the future, you can restore the database to the state captured in this file.</p>
                                </Grid>
                            </Grid>

                            <Grid style={{ marginTop: '10px' }} container alignContent='center' alignItems='center' justify='center'>
                                <Grid item justify='center' >
                                    <Button
                                        color='primary'
                                        variant='contained'
                                        href={APICall.to(Endpoint.Backup, 'create').getUrl()}
                                        startIcon={<GetAppRounded />}
                                        onClick={(e) => this.setState({
                                            showSnackbar: true,
                                            snackbarText: "Your file will be downloaded shortly."
                                        })}
                                    >Download</Button>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>

                    <Snackbar
                        open={this.state.showSnackbar}
                        autoHideDuration={6000}
                        onClose={(e) => this.setState({ showSnackbar: false })}
                        message={this.state.snackbarText}
                    />
                </GlobalThemeProvider>
            </div>
        </div>;
    }
}

export default BackupPage;