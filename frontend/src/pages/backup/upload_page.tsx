import React from 'react';
import NavBar from '../../components/common/navbar';
import { APICall, Endpoint } from '../../util/apiCall';
import { Button, Snackbar } from '@material-ui/core';
import { Grid } from '@material-ui/core';
import GlobalThemeProvider from '../../components/common/themeProvider';
import { PublishRounded } from '@material-ui/icons';
import { FilePicker } from 'react-file-picker';

type UploadPageProps = {}

type UploadPageState = {
    showSnackbar: boolean;
    snackbarText: string;
    file: any;
}

const defaultState: UploadPageState = {
    showSnackbar: false,
    snackbarText: "",
    file: "",
}

export class UploadPage extends React.Component<UploadPageProps, UploadPageState> {

    constructor(props: any) {
        super(props);

        this.state = defaultState;

        this.handleUploadFile = this.handleUploadFile.bind(this);
    }

    handleUploadFile() {
      if (!process.env.REACT_APP_BACKEND_API)
        throw "Missing backend api url in environment variables";
      const BACKEND_URL = process.env.REACT_APP_BACKEND_API;

      var data = new FormData();
      data.append("file", this.state.file);

        fetch(BACKEND_URL + '/uploads/parse', {
            method: 'POST',
            body: data,
          }).then( () => {
            this.setState({
                showSnackbar: true,
                snackbarText: "Upload successful",
                file: "",
            });
          }).catch( (e) => {
            this.setState({
                showSnackbar: true,
                snackbarText: "An error occurred while uploading the file",
            });
          });
    }

    render() {
        return <div>
            <NavBar />
            <br />
            <div className={'page'}>
                <GlobalThemeProvider>
                    <Grid container direction="column">
                        <Grid container direction="row" alignContent='center' alignItems='center'>
                            <Grid container direction="column" alignContent='center' alignItems='center'>
                                <Grid item>
                                    <h2>Upload Your Backup Data</h2>
                                </Grid>
                                <Grid item>
                                    <p style={{ margin: '10px' }}>The uploaded file should be a file that has been downloaded in the "Backup Your Data"
                                    section of the site. If it is not in that format, it may not execute or it may not execute correctly. In order to
                                    assure non-duplication of data and database integrity, the current database, if there is anything in it, will be 
                                    wiped and replaced with the data provided in the uploaded file.</p>
                                </Grid>
                            </Grid>

                            <Grid style={{ marginTop: '10px' }} container alignContent='center' alignItems='center' justify='center'>
                                <Grid item justify='center' >
                                    { this.state.file != "" ?
                                    <Button
                                        color='primary'
                                        variant='contained'
                                        startIcon={<PublishRounded />}
                                        onClick={this.handleUploadFile}
                                    >Upload</Button> : null
                                    }
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>

                    <div style={{ marginTop: '20px' }} >
                      <FilePicker
                        extensions={['rb']}
                        onChange={(FileObject:any) => (this.setState({file: FileObject}))}
                        onError={() => this.setState({
                            showSnackbar: true,
                            snackbarText: "An error occured while loading the file",
                        })}
                      >
                        <span>
                            <button>
                              Select File
                            </button>
                            <label style={{ marginLeft: '20px' }}>{this.state.file != "" ? this.state.file.name : null}</label>
                        </span>
                      </FilePicker>
                    </div>

                    <Snackbar
                        open={this.state.showSnackbar}
                        autoHideDuration={6000}
                        onClose={(e) => this.setState({ showSnackbar: false })}
                        message={this.state.snackbarText}
                    />
                </GlobalThemeProvider>
            </div>
        </div>;
    }
}

export default UploadPage;