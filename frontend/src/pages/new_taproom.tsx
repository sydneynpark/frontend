import React from 'react';
import NavBar from '../components/common/navbar';
import Grid from  '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import AddTaproomModal from '../components/add_taproom_modal';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import "../css/components/add_taproom_modal.css";
import "../css/pages/new_taproom.css"
import { APICall, Endpoint } from '../util/apiCall';

class NewBeveragePage extends React.Component<{}, {show: boolean, showSnackbar: boolean, snackbarText: string, taprooms: {id: number, name: string}[]}> {

	constructor(props=[]) {
		super(props);
		this.state = {
			show: false,
			showSnackbar: false,
			snackbarText: "",
			taprooms: [],
		};
		this.showInitialModal = this.showInitialModal.bind(this);
		this.hideModal = this.hideModal.bind(this);
		this.showErrorTaproomCreation = this.showErrorTaproomCreation.bind(this);
		this.handleDeleteTaproom = this.handleDeleteTaproom.bind(this);
		this.successfulDeletion = this.successfulDeletion.bind(this);
		this.unsuccessfulDeletion = this.unsuccessfulDeletion.bind(this);


	APICall.to(Endpoint.Groupings).get()
      .then(response => response.json())
      .then(data =>
        this.setState({taprooms: data}))
      .catch(err =>  this.setState({showSnackbar: true, snackbarText: "There was a network error retrieving taprooms from the database"}));

	}

	showInitialModal() {
		this.setState({
			show: !this.state.show
		});
	}

	hideModal() {
	  this.setState({
	    show: false,
	    showSnackbar: true,
	    snackbarText: "Taproom was created"
	  });
	}

	showErrorTaproomCreation() {
		this.setState({
			show: false,
			showSnackbar: true,
			snackbarText: "Error creating Taproom"
		});
	}

	successfulDeletion() {
		this.setState({
			show: false,
			showSnackbar: true,
			snackbarText: "Taproom successfully deleted"
		});
	}

	unsuccessfulDeletion() {
		this.setState({
			show: false,
			showSnackbar: true,
			snackbarText: "Error deleting Taproom"
		});
	}

	handleDeleteTaproom(name: string, id: number) {

		APICall.to(Endpoint.Groupings).delete(id)
		.then(() => {
			this.successfulDeletion();
		}).catch((e) =>{
			this.unsuccessfulDeletion();
		});
	}



	render() {
		let createdTaprooms = this.state.taprooms.map(tr =>
  			<div id="taproom-cards" key={tr.id}> {tr.name} <DeleteIcon id="taproom-delete-icon" onClick={() => this.handleDeleteTaproom(tr.name, tr.id)}/></div>
		);

		return <div>
		<NavBar/>
		<Grid container spacing={2} direction="row" id="grid-container">
			<Grid item xs={12}>
				<h2>Create a New Taproom:</h2>
				<Button variant="contained" onClick={this.showInitialModal}>Add Taproom</Button>
			</Grid>
			<Grid item xs={12}>
				<AddTaproomModal show={this.state.show} successfulCreation={this.hideModal} unsuccessfulCreation={this.showErrorTaproomCreation}/>
				<h3>Current Taprooms:</h3>
				{createdTaprooms.length === 0? <div><h4>You currently have no Taprooms saved.</h4></div> : createdTaprooms}
			</Grid>
		</Grid>
		<Snackbar
	        open={this.state.showSnackbar}
	        autoHideDuration={6000}
	        onClose={(e) => this.setState({showSnackbar: false})}
	        message={this.state.snackbarText}
	        action={
	          <React.Fragment>
	            <IconButton size="small" aria-label="close" color="inherit" onClick={(e) => this.setState({showSnackbar: false})}>
	            </IconButton>
	          </React.Fragment>
	        }
      	/>
		</div>
	}
}
export default NewBeveragePage
