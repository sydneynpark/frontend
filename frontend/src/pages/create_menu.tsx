import React from 'react';
import NavBar from '../components/common/navbar';
import Grid from "@material-ui/core/Grid"
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import "../css/pages/create_menu.css";
import BeverageSelector from "../components/createMenu/beverageSelector"
import Modal from '@material-ui/core/Modal';
import "../css/components/createMenu/beverageSelector.css";
import {APICall, Endpoint} from '../util/apiCall';
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";

type TapcardProps = {
  number: string,
  name: string,
  tapID: number
}

class CreateMenuPage extends React.Component<{},{
    taprooms: {id: number, name: string} [],
    taps: {id: number, beverage_id: number, grouping_id: number, beverage:{name:string}} [],
    successfulRequest:boolean,
    showSnackbar:boolean,
    snackbarText: string,
    currentTaproom: number,
    show_tap_menu: boolean,
    show_url: boolean,
    BACKEND_URL: string,
    themes: {id: number, name: string, description: string} [],
    currentTheme: number,
    menu_id: number
}>{

    getBevNameFromID(beverage_id: number){
        let bevname = "";
        fetch(this.state.BACKEND_URL + '/beverages/'+ beverage_id, {method: 'GET'})
          .then(response => response.json())
          .then(data =>{
            bevname = data.name;
            })
          .catch(err =>  this.setState({showSnackbar: true, snackbarText: "There was a network error retrieving taps"}));
          return bevname;
    };

    removeKeg(bev_to_group_id: number){
        fetch(this.state.BACKEND_URL + '/beverage_to_groupings/' + bev_to_group_id, {method: 'DELETE'})
          .then(() =>{
              this.refreshTaps(this.state.currentTaproom);
              this.setState({showSnackbar: true, snackbarText: "Keg successfully removed"})
            })
          .catch(err =>  this.setState({showSnackbar: true, snackbarText: "There was a network error blowing the keg"}));
    };


      handleRoomChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({currentTaproom: +e.target.value});
        this.refreshTaps(+e.target.value); // + sign says to use the string e.target.value as a number
      };

    refreshTaps(bev_to_group_id: number){
        // console.log("Refreshing taps for group: " + bev_to_group_id);
        const BACKEND_URL = process.env.REACT_APP_BACKEND_API;
        fetch(BACKEND_URL + '/bev_names_for_group/' + bev_to_group_id, {method: 'GET'})
          .then(response => response.json())
          .then(data =>{
          this.setState({taps: data, show_tap_menu: false});
         // console.log(this.state);
            })
          .catch(err =>  this.setState({showSnackbar: true, snackbarText: "There was a network error retrieving beverages on tap"}));
    }

    closeSelector(success: boolean){
        this.setState({show_tap_menu: false});
        if(success){
             this.setState({showSnackbar: true, snackbarText: "Beverage tapped"});
        }else{
             this.setState({showSnackbar: true, snackbarText: "Failed to tap beverage"});
        }
        this.refreshTaps(this.state.currentTaproom);

    }

    handleThemeChange = (e: React.ChangeEvent<HTMLInputElement>) => {
          this.setState({currentTheme: +e.target.value});
    };

    checkExistingURL(){
        let currentThemeId = this.state.currentTheme;
        let currentTaproomId = this.state.currentTaproom;
        let data = {menu: {menu_config_id: currentThemeId, grouping_id: currentTaproomId}};

        fetch(
                this.state.BACKEND_URL + '/menus', 
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(data)
                }
            )
            .then(response => response.json())
            .then(data => {
                this.setState({menu_id: data.id, show_url: true});
            })
            .catch(err => this.setState({showSnackbar: true, snackbarText: "There was a network error retrieving menus"} ));
    }

    closeUrlModal(){
        this.setState({show_url: false});
    }

    constructor(props =[]){
        super(props);
        if (!process.env.REACT_APP_BACKEND_API)
            throw "Missing backend api url in environment variables";
        const BACKEND_URL = process.env.REACT_APP_BACKEND_API;

        this.state = {
            taprooms: [],
            taps: [],
            successfulRequest: false,
            showSnackbar: false,
            snackbarText: "",
            currentTaproom: -1,
            BACKEND_URL: BACKEND_URL,
            show_tap_menu: false,
            show_url: false,
            menu_id: -1,
            themes: [],
            currentTheme: -1
        };
        this.checkExistingURL = this.checkExistingURL.bind(this);
        this.getBevNameFromID = this.getBevNameFromID.bind(this);
        this.closeSelector = this.closeSelector.bind(this);
        this.closeUrlModal = this.closeUrlModal.bind(this);

    fetch(BACKEND_URL + '/groupings/', {method: 'GET'})
      .then(response => response.json())
      .then(data =>{
        this.setState({taprooms: data, show_tap_menu: false, currentTaproom: data[0].id});
        this.refreshTaps(this.state.currentTaproom);

        })
      .catch(err =>  this.setState({showSnackbar: true, snackbarText: "There was a network error retrieving taprooms"}));

    APICall.to(Endpoint.MenuConfigs).get()
        .then(response => response.json())
        .then(data =>{
            //console.log(data);
            this.setState({themes: data});
        })
        .catch(err => this.setState({showSnackbar: true, snackbarText: "There was a network error retrieving themes"}));

    }

  render() {
      const TaproomTapCard = ({ number, name, tapID }: TapcardProps) =>
      (
          <Card className="bev-card" variant="outlined" id= {name === "Empty"? "tapKegCard" : "" }>
            <CardContent>
            <Grid container direction="row" justify="space-around" alignItems="center">
                <h3 style={name ==='Empty'? {color: 'red'} : {color: 'black'}}>{name}</h3>
                <CardActions>
                    { name !== "Empty" ? <div>
                          <Button id={'blowButton-'+tapID} className="bevCardButton" size="small" color="secondary" variant='contained' onClick={() => this.removeKeg(tapID)}>Blow keg</Button>
                          </div>
                          : null
                    }
                    { name === "Empty" ? <Button className="bevCardButton" size="small" color="primary" variant='contained' onClick={() => this.setState({show_tap_menu:true})}>Tap keg</Button>
                                      : null
                    }
                </CardActions>
            </Grid>
            </CardContent>
          </Card>
      );



      let taproomOptionItems = this.state.taprooms.map((taproom) =>
        <MenuItem key={taproom.id} value={taproom.id}>{taproom.name}</MenuItem>
      );

      //console.log(this.state.taprooms);

      let taps = this.state.taps.map((tap) =>
      <TaproomTapCard number={"" + tap.id} name={"" + tap.beverage.name + this.getBevNameFromID(tap.beverage_id)} tapID={tap.id}/>
        );

      let themeOptionItem = this.state.themes.map((theme) =>
          <MenuItem key={theme.id} value={theme.id}>{theme.name}</MenuItem>
      );
    return <div>
    <NavBar/>
        <Dialog
          aria-labelledby="Tap a beverage"
          open={this.state.show_tap_menu}
          onClose={() => this.refreshTaps(this.state.currentTaproom)}>
          <BeverageSelector taproomID={this.state.currentTaproom} onClose={this.closeSelector}/>
        </Dialog>
    <Grid container direction="column" justify="center" alignItems="center">
    <Grid id="taproom_grid" container direction="row" justify="center" alignItems="center">
        <h2>Taproom:</h2>
        <TextField id="taproom_select" label="Taproom" variant="outlined" onChange={this.handleRoomChange} value={this.state.currentTaproom} select>
        {taproomOptionItems}
        </TextField>
    </Grid>
    <Grid id="theme_grid" container direction="row" justify="center" alignItems="center">
        <h2>Theme:</h2>
        <TextField id="theme_select" label="Theme" variant="outlined" onChange={this.handleThemeChange} select>
            {themeOptionItem}
        </TextField>
    </Grid>
        <Button id="url_button" size="small" color="primary" variant="contained" onClick={this.checkExistingURL}>Generate URL</Button>
        <Dialog open={this.state.show_url} onClose={this.closeUrlModal} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description">
            <DialogTitle id="alert-dialog-title">Menu URL:</DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-description"><a href={window.location.href.replace("/taprooms","/menu/") + this.state.menu_id} target="_blank">{window.location.href.replace("/taprooms","/menu/") + this.state.menu_id}</a></DialogContentText>
            </DialogContent>
        </Dialog>
    <h2>Beverages on tap:</h2>
    </Grid>
    <Grid container direction="column" justify="center" alignItems="center">
        {taps}
        <TaproomTapCard number={""+ taps.length} name="Empty" tapID={-1}/>

    </Grid>

    <Snackbar
      open={this.state.showSnackbar}
      autoHideDuration={6000}
      onClose={(e) => this.setState({showSnackbar: false})}
      message={this.state.snackbarText}
      action={
        <React.Fragment>
          <IconButton size="small" aria-label="close" color="inherit" onClick={(e) => this.setState({showSnackbar: false})}>
          </IconButton>
        </React.Fragment>
      }
    />

    </div>

    }
}
export default CreateMenuPage
