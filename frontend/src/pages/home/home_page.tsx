import React from 'react';
import NavBar from '../../components/common/navbar';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import "../../css/pages/home_page.css";
import { Link } from 'react-router-dom';
import BeverageList from '../../components/home/beverage_list';

class HomePage extends React.Component<{}, {showSnackbar: boolean, snackbarText: string}> {

  constructor(props = []) {
    super(props);
    this.state = {
      showSnackbar: false,
      snackbarText: ""
    };

    this.handleDeletedBeverage = this.handleDeletedBeverage.bind(this);
    this.handleUnsuccessfulBevDeletion = this.handleUnsuccessfulBevDeletion.bind(this);
  }

  handleDeletedBeverage(){
     this.setState({
       showSnackbar: true,
       snackbarText: "Beverage successfully deleted"
     });
  }

  handleUnsuccessfulBevDeletion() {
    this.setState({
      showSnackbar: true,
      snackbarText: "There was an issue deleting the beverage"
    });
  }

  render() {
    return <div>
    <NavBar/>
    <h1 id="homePageTitle">Your Beverages</h1>
    <BeverageList handleDeletedBeverage={this.handleDeletedBeverage} handleUnsuccessfulBevDeletion={this.handleUnsuccessfulBevDeletion}/>
    <Link to="/newBeverage">
    <Fab aria-label="add" id="addFab">
      <AddIcon />
    </Fab>
    </Link>
    <Snackbar
      open={this.state.showSnackbar}
      autoHideDuration={6000}
      onClose={(e) => this.setState({showSnackbar: false})}
      message={this.state.snackbarText}
      action={
        <React.Fragment>
          <IconButton size="small" aria-label="close" color="inherit" onClick={(e) => this.setState({showSnackbar: false})}>
          </IconButton>
        </React.Fragment>
      }
    />
    </div>
  }
}
export default HomePage
