
// URL of the API should exist as part of the environment
if (!process.env.REACT_APP_BACKEND_API)
    throw "Missing backend api url in environment variables";
const BACKEND_URL = process.env.REACT_APP_BACKEND_API;

export enum Endpoint {
    Backup = 0,
    BeverageTypes,
    Beverages,
    Groupings,
    KegSizes,
    KegStates,
    Kegs,
    MenuConfigs,
    MenuConfigOptions,
    MenuConfigValues,
    Menus,
    PourPrices,
    PourSizes,
    Producers,
    Users,
}

/**
 * Helper class for making API calls to the backend.
 * This class follows the factory pattern (I think?) to create an APICall object, and 
 * then returns a Promise<Result> from when you actually send the request.
 * 
 * USAGE: 
 *  * To create an API Call, call the static {@link APICall.to} method.
 *      - Pass in the Endpoint enum corresponding to which endpoint on the backend you want to hit
 *      - If there is a separate suburl you want to use, pass that as the second argument
 *              (example: users/login and users/create on the users endpoint)
 *  * Optionally call {@link APICall.suppressError} to specify that you don't want any error 
 *      handling and that you will check the OK status of the response yourself.
 *  * Optionally call {@link APICall.withOptions} if there are any options you wish to pass
 *      to the {@link fetch} method. These will be applied to the second parameter of the fetch call.
 *  * Once you are ready to send the request, call one of the following methods. They will send 
 *      the request, check the OK status of the response (if error checking is enabled), 
 *      and return to you the Promise<Response> object returned from the fetch call.
 *      If the status of the response is not OK, it will throw an exception instead.
 *      
 *      Methods to send the request:
 *      - get() - gets all items from the endpoint
 *      - getById(id) - gets a single item from the endpoint
 *      - post(data)
 *      - patch(data)
 *      - delete(id)
 */
export class APICall {

    /**
     * URL to which the FETCH call will ultimately be sent
     */
    private url: string;

    /**
     * Setting for whether to check the OK response. 
     * If true, when a fetch call receives a response with OK status set to false, 
     *      it will throw an exception. 
     * If false, the OK response will not be checked.
     */
    private handleError: boolean = true;

    /**
     * Any options that will be applied to the FETCH call as the second parameter.
     * Objects passed in through {@link APICall.withOptions} get copied to here.
     */
    private options: RequestInit = {
    };

    private headers: Headers | null = null;

    /**
     * One central place to store all of the backend endpoints and their URL strings
     * so that we do not have to put hard-coded strings into our requests.
     * Instead we can just refer to the endpoint enums.
     */
    static EndpointUrls: Map<Endpoint, string> = new Map([
        [ Endpoint.Backup, 'backup'],
        [ Endpoint.BeverageTypes, 'beverage_types' ],
        [ Endpoint.Beverages, 'beverages'],
        [ Endpoint.KegSizes, 'keg_sizes' ],
        [ Endpoint.Groupings, 'groupings' ],
        [ Endpoint.KegStates, 'keg_states' ],
        [ Endpoint.Kegs, 'kegs' ],
        [ Endpoint.MenuConfigs, 'menu_configs' ],
        [ Endpoint.MenuConfigOptions, 'menu_config_options' ],
        [ Endpoint.MenuConfigValues, 'menu_config_values' ],
        [ Endpoint.Menus, 'menus' ],
        [ Endpoint.PourPrices, 'pour_prices' ],
        [ Endpoint.PourSizes, 'pour_sizes' ],
        [ Endpoint.Producers, 'producers' ],
        [ Endpoint.Users, 'users' ],
    ]);

    private constructor(endpoint: Endpoint, customSubUrl?: string) {
        this.url = `${BACKEND_URL}/${APICall.EndpointUrls.get(endpoint)}`;

        if (customSubUrl) {
            this.url += `/${customSubUrl}`;
        }
    }


    /** BUILD YOUR OWN HTTP REQUEST **/

    /**
     * Constructs an API call to the backend at the URL specified.
     * 
     * NOTE - call suppressError() if you want to check the response's OK status yourself.
     * Otherwise it will be checked automatically and will throw an Error if the response is not OK.
     * @param endpoint Which endpoint of the backend API to call
     * @param customSubUrl Any additional url you wish to send the API to
     * @example http://beer_menu_api.com/endpoint/customSubUrl is how the URL will be formatted
     */
    public static to(endpoint: Endpoint, customSubUrl?: string): APICall {
        return new APICall(endpoint, customSubUrl);
    }

    /**
     * Do not throw an error if the status of the response is not OK.
     * By default, the response status will be checked and will throw an error if status is not OK.
     */
    public suppressError(): APICall {
        this.handleError = false;
        return this;
    }

    /**
     * Provide additional options to the FETCH request that will be created
     * @param options Any options you wish to apply to the fetch call.
     */
    public withOptions(options: RequestInit): APICall {
        this.options = Object.assign(this.options, options);
        return this;
    }

    /**
     * Provide a header to the FETCH request that will be created
     * @param header The header you wish to set
     * @param value The value to set on that header
     */
    public withHeader(header: string, value: string): APICall {
        if (this.headers == null) {
            this.headers = new Headers();
        }
        this.headers.append(header, value);

        return this;
    }


    /** SEND YOUR HTTP REQUEST **/

    /**
     * Sends a GET ALL request
     */
    public get(): Promise<Response> {
        return this.sendRequest('GET');
    }

    /**
     * Sends a GET request for a single item
     * @param id the ID of the item to retrieve
     * @example http://beer_menu_api.com/endpoint/id
     */
    public getById(id: number): Promise<Response> {
        this.targetSpecificItem(id);
        return this.sendRequest('GET');
    }

    /**
     * Sends a POST request to create a new entity
     * @param body Information about the item to be created
     */
    public post(body: any): Promise<Response> {
        return this.sendRequest('POST', body);
    }

    /**
     * Sends a PATCH request to update an existing entity
     * @param id ID of the item to update
     * @param body Information to be updated
     */
    public patch(id: number, body: any): Promise<Response> {
        this.targetSpecificItem(id);
        return this.sendRequest('PATCH', body);
    }

    public put(id: number, body: any): Promise<Response> {
        this.targetSpecificItem(id);
        return this.sendRequest('put', body);
    }

    /**
     * Deletes a specific item from the backend
     * @param id ID of the item to be deleted
     */
    public delete(id: number): Promise<Response> {
        this.targetSpecificItem(id);
        return this.sendRequest('DELETE');
    }


    /** Public helpers **/
    
    public getUrl(): string {
        return this.url;
    }

    /** Private helpers **/

    private targetSpecificItem(id: number) {
        this.url += `/${id}`;
    }

    private sendRequest(httpMethod: string, requestBody?: any): Promise<Response> {
        this.options.method = httpMethod;
        if (requestBody) {
            this.options.body = requestBody;
        }
        if (this.headers) {
            this.options.headers = this.headers;
        }
        
        var responsePromise = fetch(this.url, this.options);
        
        return this.errorCheck(responsePromise);
    }

    /**
     * If {@see APICall.handleError} is true, then unwrap the response and make sure status is OK.
     * @param responsePromise The response that was returned from the FETCH call.
     * @returns The responsePromise, or a thrown exception
     * @throws Error when the response had a non-OK status.
     */
    private errorCheck(responsePromise: Promise<Response>): Promise<Response> {
        if (this.handleError) {
            return responsePromise.then(response => {
                if (!response.ok) {
                    throw new Error(`HTTP request to ${this.url} FAILED with status ${response.status}`);
                }
                return responsePromise;
            });
        } else {
            return responsePromise;
        }
    }
}

export default {APICall, Endpoint};