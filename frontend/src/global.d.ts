//#region Type Definitions from the backend and database //

type beverage = {
    id:  number, 
    abv: number,
    name: string, 
    color: string, 
    description: string,
    beverage_type: beverage_type
    beverage_type_id: number
    pour_prices: pour_price[]
    producer: producer
}

type producer = {
    id: number,
    name: string,
    description: string
}

type beverage_type = {
    id: number
    type_name: string,
    description: string
}

type beverage_grouping = {
    id: number
    name: string
    beverages: beverage[]
}

type pour_price = pour_price_attributes & {
    id?: number
    beverage_id?: number
    pour_size?: pour_size
}

type pour_price_attributes = {
    pour_price: number,
    pour_size_id?: number
    pour_size_attributes?: pour_size_attributes
}

type pour_size = pour_size_attributes & {
    id?: number
}

type pour_size_attributes = {
    size: string
}

type config_item = {
    id: number,
    name: string,
    datatype: string,
    default_value: any
    value: any
}

type config_info = {
    id: number,
    name: string,
    description: string
}

type config_with_items = {
    menu_config_data: config_info,
    menu_config_options: config_item[]
}

//#endregion ////////////////////////////////////////////////

type validation_error = {
    error: boolean, 
    message: string
}

// Menu style object that is used by the menu component to generate a menu
// If you are adding an attribute, then you need to update the following:
//      * this type defintion
//      * the menu component
//      * the menuTypeMap
type menu_style_options = {
    display_type: display_type
} & card_style & grid_style 

type base_menu_style = {
    use_background_image: boolean,
    background_image?: string,
    background_color: string

    title_font: string
    text_font: string

    title: string

    include_description: boolean
    include_pours: boolean
    include_producers: boolean
} 

type card_style = {
    rows: number 
    columns: number
    beverage_card_color: string
} & base_menu_style

type grid_style = {
    table_header_color: string
} & base_menu_style

type display_type = "grid" |"card";