import React from 'react';
import { RedirectProps } from 'react-router-dom';
import { GoogleLoginProps } from 'react-google-login';
import { TwoOptionDialogProps } from './components/common/two_option_dialog';
import { AlertProps } from './components/common/alert';

global.fetch = fetch;

jest.mock('./components/environment/EnvironmentManager.tsx', () => {
    return {
      retrieveGoogleKey(){
        return "this is the test key"
      }
    }
});

jest.mock('react-google-login', () => {
  var mockGoogleLogin: React.FC<GoogleLoginProps> = (props) => {
    return <div></div>;
  }
  return {
    GoogleLogin: mockGoogleLogin
  };
});

jest.mock('./components/common/two_option_dialog.tsx',  () => {
  var mockTwoOptionDialog: React.FC<TwoOptionDialogProps> = (props: TwoOptionDialogProps) => {
    return <div className={'TwoOptionDialog'}></div>;
  }
  return {
    __esModule: true,
    default: mockTwoOptionDialog,
  }
});

jest.mock('./components/common/alert.tsx',  () => {
  var mockAlert: React.FC<AlertProps> = (props: AlertProps) => {
    return <div className={'Alert'}></div>;
  }
  return {
    __esModule: true,
    default: mockAlert,
  }
});

jest.mock('react-router-dom', () => {
  var mockRedirect: React.FC<RedirectProps> = () => {
    return <div className={"Router"} ></div>;
  }

  function fake(post: string) {

  }

  return {
    Redirect: mockRedirect,
    useHistory: () => { return {push: fake}}
  };
})

jest.mock('@material-ui/core', () => {
  return {
    Dialog: (props: any) => <div id={props.id || 'Dialog'}>{props.children}</div>,
    Snackbar: (props: any) => <div id={props.id || 'Snackbar'}>{props.children}</div>,
    IconButton: (props: any) => <div id={props.id || 'IconButton'}>{props.children}</div>,
    DialogTitle: (props: any) => <div id={props.id || 'DialogTitle'}>{props.children}</div>,
    DialogContent: (props: any) => <div id={props.id || 'DialogContent'}>{props.children}</div>,
    DialogActions: (props: any) => <div id={props.id || 'DialogActions'}>{props.children}</div>,
    DialogContentText: (props: any) => <div id={props.id || 'DialogContentText'}>{props.children}</div>,
    Button: (props: any) => <div id={props.id || 'Button'}>{props.children}</div>,
    TextField: (props: any) => <div id={props.id || 'TextField'}>{props.children}</div>,
    TableCell: (props: any) => <div id={props.id || 'TableCell'}>{props.children}</div>,
    TableRow: (props: any) => <div id={props.id || 'TableRow'}>{props.children}</div>,
    Table: (props: any) => <div id={props.id || 'Table'}>{props.children}</div>,
    TableBody: (props: any) => <div id={props.id || 'TableBody'}>{props.children}</div>,
    TableContainer: (props: any) => <div id={props.id || 'TableContainer'}>{props.children}</div>,
    TableHead: (props: any) => <div id={props.id || 'TableHead'}>{props.children}</div>,
    InputAdornment: (props: any) => <div id={props.id || 'TableHead'}>{props.children}</div>,
    AppBar: (props: any) => <div id={props.id || 'TableHead'}>{props.children}</div>,
    Toolbar: (props: any) => <div id={props.id || 'TableHead'}>{props.children}</div>,
    Typography: (props: any) => <div id={props.id || 'TableHead'}>{props.children}</div>,
    Switch: (props: any) => <div id={props.id || 'TableHead'}>{props.children}</div>,
    FormControlLabel: (props: any) => <div id={props.id || 'TableHead'}>{props.children}</div>,
    FormGroup: (props: any) => <div id={props.id || 'TableHead'}>{props.children}</div>,
    MenuItem: (props: any) => <div id={props.id || 'TableHead'}>{props.children}</div>,
    Menu: (props: any) => <div id={props.id || 'TableHead'}>{props.children}</div>,
  };
})

jest.mock('@material-ui/lab', () => {
  return {
    AutoComplete: (props: any) => <div id={props.id || 'Dialog'}>{props.children}</div>,
  };
})

jest.mock('@material-ui/icons', () => {
  return {
    AccountCircle: (props: any) => <div id={props.id || 'Dialog'}>{props.children}</div>,
    Delete: (props: any) => <div id={props.id || 'TableHead'}>{props.children}</div>,
  };
})

jest.mock('react-device-detect', () => {
  return {
    isMobile: () => false,
  };
})