import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import NewTaproomPage from './pages/new_taproom'
import { Route, BrowserRouter as Router, Redirect } from 'react-router-dom'
import Home from './pages/home/home_page'
import NewBeveragePage from './pages/new_beverage'
import CreateMenuPage from './pages/create_menu'
import * as serviceWorker from './serviceWorker'
import Login from './pages/login/login_page'
import EditBeveragePage from './pages/edit_beverage'
import MenuPage from './pages/menu_page'
import CreateMenuStylePage from './pages/menu_style/new_style_page';
import BackupPage from './pages/backup/backup_page'
import EditMenuStylePage from './pages/menu_style/edit_style_page'
import ViewMenuStylesPage from './pages/menu_style/view_styles_page'
import UploadPage from './pages/backup/upload_page'


function ProtectRoute (loggedInComponent: any, notLoggedInComponent: any = <Redirect to={'/login'}/>) {
  return sessionStorage.getItem('user') != null ? loggedInComponent : notLoggedInComponent
}

const routing = (
  <Router>
    <div style={{height: '100%'}}>
    <Route path="/taprooms" >
      {ProtectRoute(<CreateMenuPage/>)}
    </Route>
      <Route exact path="/">
        <Redirect to={'/login'}/>
      </Route>
      <Route path="/home" >
        {ProtectRoute(<Home/>)}
      </Route>
      <Route path="/newBeverage" >
        {ProtectRoute(<NewBeveragePage/>)}
      </Route>
      <Route path="/editBeverage/:beverage_id" component={ProtectRoute(EditBeveragePage)} />
      <Route path="/login" >
        {ProtectRoute(<Redirect to={'/home'}/>, <Login/>)}
      </Route>
      <Route path="/menu/:menu_id" component={ProtectRoute(MenuPage)}/>
      <Route path="/newTaproom" >
      	{ProtectRoute(<NewTaproomPage/>)}
      </Route>
      <Route path="/menuStyle">
        {ProtectRoute(<CreateMenuStylePage/>)}
      </Route>
      <Route path="/editMenuStyle/:style_id">
        {ProtectRoute(<EditMenuStylePage/>)}
      </Route>
      <Route path="/menuStyles">
        {ProtectRoute(<ViewMenuStylesPage/>)}
      </Route>
      <Route path="/backup">
        {ProtectRoute(<BackupPage/>)}
      </Route>
      <Route path="/upload">
        {ProtectRoute(<UploadPage/>)}
      </Route>
      </div>
  </Router>
)
ReactDOM.render(routing, document.getElementById('root'))
document.getElementById('root')?.style.setProperty('height', '100%')

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
