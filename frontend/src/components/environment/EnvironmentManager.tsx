import CryptoJS from 'crypto-js'

var EnvironmentManager = {
    retrieveGoogleKey(): string {
        var encrypted = process.env.REACT_APP_GOOGLE_CLIENT_ID;
        return this.decrypt(encrypted);
    },
    
    decrypt(encrypted: string | undefined): string{
        if(process.env.REACT_APP_ENCRYPTION_KEY !== undefined && encrypted !== undefined){

            var key = CryptoJS.enc.Hex.parse(process.env.REACT_APP_ENCRYPTION_KEY);

            var decrypted = CryptoJS.AES.decrypt(
                encrypted, 
                key, 
                {iv: CryptoJS.enc.Hex.parse('00000000000000000000000000000000'),
                mode: CryptoJS.mode.CBC});

            return decrypted.toString(CryptoJS.enc.Utf8);

        }else{
            throw "Missing environment variables"
        }
    },
};

export default EnvironmentManager;