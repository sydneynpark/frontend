import React from 'react';
import Grid from "@material-ui/core/Grid"
import TextField from "@material-ui/core/TextField";
import Button from '@material-ui/core/Button';
import MenuItem from "@material-ui/core/MenuItem";
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import "../../css/components/createMenu/beverageSelector.css";
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';



type BevProps = {
  name: string,
  bevID: number,
}

type BevSelectorProps = {
    taproomID: number,
    onClose: any
}

class BeverageSelector extends React.Component<BevSelectorProps,
 {
     all_beverages: { id: number, name: string} [],
     displayed_beverages: { id: number, name: string} [],
     search_term: string,
     taproomID: number,
     onClose: any
 }> {

  tapKeg(bevID: number){
      if (!process.env.REACT_APP_BACKEND_API)
          throw "Missing backend api url in environment variables";
      const BACKEND_URL = process.env.REACT_APP_BACKEND_API;

      var data = new FormData();

      data.append("beverage_id", ""+bevID);
      data.append("grouping_id", ""+this.state.taproomID);

      fetch(BACKEND_URL + '/beverage_to_groupings/', {
          method: 'POST',
          body: data
      })
      // Successfully saved beverage
      .then(() => {
          this.state.onClose(true);
        })
      .catch((err) => {
          this.state.onClose(false);
        });
  }

  constructor(props : BevSelectorProps) {
    super(props);
    this.state = {
        all_beverages: [],
        displayed_beverages: [],
        search_term: "",
        taproomID: props.taproomID,
        onClose: props.onClose
    };

    if (!process.env.REACT_APP_BACKEND_API)
        throw "Missing backend api url in environment variables";
    const BACKEND_URL = process.env.REACT_APP_BACKEND_API;


    fetch(BACKEND_URL + '/beverages/', {method: 'GET'})
      .then(response => response.json())
      .then(data => {
        this.setState({all_beverages: data, displayed_beverages: data});
        })
      .catch(err =>  console.error("Failed to retrieve all beverages"));

  }

  filterByString = (data:any, s:string) => {
     return data.filter((e:any) => e.name.includes(s))
         .sort((a:any,b:any) => a.name.includes(s) && !b.name.includes(s) ? -1 : b.name.includes(s) && !a.name.includes(s) ? 1 :0);
  }

  handleFilterChange = (e: React.SyntheticEvent) => {
    const term = (e.target as HTMLInputElement).value;
    this.setState({search_term: term});
    this.setState({displayed_beverages: this.filterByString(this.state.all_beverages, (e.target as HTMLInputElement).value)});
  }

  render() {
      const BeverageCard = ({name, bevID }: BevProps) =>
      (
          <div className="tap-card">
            <Grid container direction="row" justify="space-around" alignItems="center">
                <h4>{name}</h4>
                    <div>
                    <Button className="tapBevButton" size="small" color="secondary" variant='contained' onClick={() => this.tapKeg(bevID)}>Tap keg</Button>
                    </div>
            </Grid>
          </div>
      );

      let allBeverages = this.state.displayed_beverages.map((bev) =>
        <BeverageCard name={bev.name} bevID={bev.id}/>
      );

    return <div id="tap-modal">
        <h3>Tap a beverage:</h3>
        <TextField value={this.state.search_term} helperText="beverage name" id="search-box" label="Beverage Name" onChange={this.handleFilterChange}/>
        <div id="tap-list">
            {allBeverages}
        </div>
      </div>
  }
}
export default BeverageSelector
