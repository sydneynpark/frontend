import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import '../../css/components/home/bev_card.css';
import { Endpoint, APICall } from '../../util/apiCall';

class BevCard extends React.Component<
  {handleSuccessfulDeletion: any, 
  handleUnsuccessfulDeletion: any, 
  handleEdit: any,
  beverage_id: number, 
  color: string, 
  description: string, 
  beverageName: string, 
  beverageType: any, 
  abv: string, 
  producer: string,
  pourPrices: {id: number, pour_price: number, pour_size: {id: number, size: string}}[]},
    {expanded: boolean}>{

  constructor(props: any) {
    super(props);

    this.state = {
      expanded: false,
    }

    this.handleExpand = this.handleExpand.bind(this);
    this.handleDeleteBeverage = this.handleDeleteBeverage.bind(this);
  }

  handleExpand = (e: React.SyntheticEvent) => {
    this.setState({
      expanded: !this.state.expanded,
    });
  }

  handleDeleteBeverage(id: number, name: string) {
    if (!process.env.REACT_APP_BACKEND_API)
      throw "Missing backend api url in environment variables";
    const BACKEND_URL = process.env.REACT_APP_BACKEND_API;

    APICall.to(Endpoint.Beverages).delete(id)
    .then(() => {
      this.props.handleSuccessfulDeletion(name);
    }).catch((e) =>{
      this.props.handleUnsuccessfulDeletion(name);
    });
  }

  render() {

  var colorBlockStyleObj = {
    'height': '50px',
    'width': '50px',
    'backgroundColor': `${this.props.color}`,
    'marginRight': '20px',
  }

  let pourPriceOptions = this.props.pourPrices.map((pp) =>
    <p className="pourPrices">${pp.pour_size.size}: ${pp.pour_price} </p>
  );
  return (
    <Card className="root" variant="outlined">
      <CardContent id="topLine">
        <span>
          <Typography variant="h5" component="h2" className="beerLine">
            {this.props.beverageName}
          </Typography>
          <Typography className="pos" color="textSecondary">
            Type: {this.props.beverageType}
          </Typography>
          <Typography className="pos" color="textSecondary" id="abv">
            ABV: {this.props.abv}%
          </Typography>
          <Typography className="pos" color="textSecondary" id="abv">
            {pourPriceOptions}
          </Typography>
        </span>
        { this.state.expanded ?
        <div>
        <div className="beerLine">
          <div style={colorBlockStyleObj}/>
          <div>
            <Typography>
               Producer: {this.props.producer}
            </Typography>
            <Typography>
               {this.props.description}
            </Typography>
          </div>
        </div>
        </div> : null
        }
      </CardContent>
      <CardActions id="expandButton">
        <span>
          {this.state.expanded ?
            <div id="hiddenButtons">
            <Button id="deleteButton" size="small" variant="outlined" color="secondary" onClick={() => this.handleDeleteBeverage(this.props.beverage_id, this.props.beverageName)}>Delete</Button>
            <Button id="editButton" size="small" variant="outlined" color="primary" onClick={() => this.props.handleEdit(this.props.beverage_id)}>Edit</Button></div> : null
          }
          <Button size="small" onClick={this.handleExpand}>{this.state.expanded ? "Collapse" : "Expand"}</Button>
        </span>
      </CardActions>
    </Card>
  );


  }
}

export default BevCard