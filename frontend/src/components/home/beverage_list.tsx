import React from 'react';
import { Endpoint, APICall } from '../../util/apiCall';
import BevCard from './bev_card';
import '../../css/components/home/bev_list.css';
import EditBeveragePage from '../../pages/edit_beverage';
import {Redirect} from 'react-router-dom';

class BeverageList extends React.Component<{handleDeletedBeverage: any, handleUnsuccessfulBevDeletion: any}, 
	{
	current_beverages:{id: number, name: string, 
		beverage_type: {id: number, type_name: string, description: string},
		producer: {id: number, name: string, description: string}, 
		pour_prices: {id: number, pour_price: number, pour_size: {id: number, size: string}}[],
		abv: any, description: string, color: string}[],
	showSnackbar: boolean, editBeverageId: any,
	snackbarText: string}> {

	constructor(props: any) {
	  super(props)

	  this.state = {
	  	current_beverages: [],
	  	showSnackbar: false,
	  	snackbarText: "",
	  	editBeverageId: undefined,
	  }

	  this.handleNetworkError = this.handleNetworkError.bind(this);
	  this.handleSuccessfulDeletion = this.handleSuccessfulDeletion.bind(this);
	  this.handleUnsuccessfulDeletion = this.handleUnsuccessfulDeletion.bind(this);
	  this.handleEdit = this.handleEdit.bind(this);
	  this.getData = this.getData.bind(this);

	  this.getData();

	}

	getData() {
	  if (!process.env.REACT_APP_BACKEND_API)
        throw "Missing backend api url in environment variables";
      const BACKEND_URL = process.env.REACT_APP_BACKEND_API;

	  fetch(BACKEND_URL + '/enhanced_beverages', {method: 'GET'})
	    .then(response => response.json())
	    .then(data => 
	    	this.setState({current_beverages: data.beverages}),
	    )
	    .catch(err => this.handleNetworkError("beverages"));
	}

	handleEdit(id: number){
    	this.setState({
    		editBeverageId: id,
    	});
  	}

	handleNetworkError = (whatFailedToRetrieve: string) => {
	    this.setState({
	      showSnackbar: true, 
	      snackbarText: `There was a network error retrieving ${whatFailedToRetrieve}`
	    });
  	}

  	handleSuccessfulDeletion(name: string) {
  		this.getData();
  		this.props.handleDeletedBeverage();
  	}

  	handleUnsuccessfulDeletion(name: string) {
  		this.props.handleUnsuccessfulBevDeletion();
  	}

	render() {
		let BeverageItems = this.state.current_beverages.map((b) =>
		  <div  className="bevCards">
		    <BevCard key={b.id} handleSuccessfulDeletion={this.handleSuccessfulDeletion} 
		    handleUnsuccessfulDeletion={this.handleUnsuccessfulDeletion} handleEdit={this.handleEdit} beverage_id={b.id} color={b.color} 
		    description={b.description} beverageName={b.name} 
		    beverageType={b.beverage_type.type_name} 
		    abv={b.abv} pourPrices={b.pour_prices} producer={b.producer.name}/>
	      </div>
	    );
		return <div id="bevList">
			{BeverageItems}
			{this.state.editBeverageId && <Redirect to={`/editBeverage/${this.state.editBeverageId}`} />}
		</div>
  	}
	
}
export default BeverageList
