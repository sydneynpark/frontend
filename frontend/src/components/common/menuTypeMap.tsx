//#region Type Mapping Functions ///////////////////////////////////////////////////////
export function asString(param: string): string {
    return param as string
}
export function asInt(param: string): number {
    return Number.parseInt(param)
}
export function asFloat(param: string): number {
    return Number.parseFloat(param)
}
export function asBoolean(param: string): boolean {
    if (param.toLowerCase() == 'true' || param.toLowerCase() == 't') {
        return true
    } else if (param.toLowerCase() == 'false' || param.toLowerCase() == 'f') {
        return false
    }
    throw "Cannot cast " + param + " to type boolean"
}

var typeMapper = new Map<string, any>();
typeMapper.set('integer', asInt);
typeMapper.set('string', asString);
typeMapper.set('float', asFloat);
typeMapper.set('boolean', asBoolean);
typeMapper.set('color', asString);
typeMapper.set('font', asString);
typeMapper.set('display_type', asString);
//#endregion //////////////////////////////////////////////////////////////////////////


// Casts a config item to its respective type. if the value is null, it uses the default value.
export function typeCastConfig(config: config_item): any {
    var castingFunction = typeMapper.get(config.datatype)
    return castingFunction(config.value??config.default_value)
}

// Maps a menu config response to a menu style object that can be used by the menu component
const mapper = {
    mapResponseToStyle(config: config_item[]) {

        // puts the menu config items into a map for easy access.
        var configNames = new Map<string, any>();
        config.forEach(element => {
            configNames.set(element.name, element);
        });

        var style: menu_style_options = {
            display_type: typeCastConfig(configNames.get('display_type')) as display_type,
            table_header_color: typeCastConfig(configNames.get('table_header_color')),
            rows: typeCastConfig(configNames.get('rows')),
            columns: typeCastConfig(configNames.get('columns')),
            use_background_image: typeCastConfig(configNames.get('use_background_image')),
            background_image: typeCastConfig(configNames.get('background_image')),
            background_color: typeCastConfig(configNames.get('background_color')),
            beverage_card_color: typeCastConfig(configNames.get('beverage_card_color')),
            title_font: typeCastConfig(configNames.get('title_font')),
            text_font: typeCastConfig(configNames.get('text_font')),
            title: typeCastConfig(configNames.get('title')),
            include_description: typeCastConfig(configNames.get('include_description')),
            include_pours: typeCastConfig(configNames.get('include_pours')),
            include_producers: typeCastConfig(configNames.get('include_producers')),
        }

        return style;
    },
};

export default mapper;