import React, { PropsWithChildren } from 'react';
import TextField, { TextFieldProps } from "@material-ui/core/TextField";
import Checkbox from "@material-ui/core/Checkbox";
import ColorPicker from '../common/colorPicker';
import FontSelectorDropdown from '../common/font-selector';
import YesNoDropdown from '../common/yesNoDropdown';
import { Badge } from '@material-ui/core';
import DisplayTypeDropdown from './displayTypeDropdown';

export function buildNumberInput(
    id: string, 
    onChange: (event: any) => void, 
    error: boolean,
    errorMessage: string,
    value?: number
    ) : JSX.Element
{
    return <TextField variant="outlined" fullWidth={true} type="number" id={id} value={value} onChange={onChange} error={error} helperText={errorMessage}/>;
}

export function buildTextInput(
    id: string, 
    onChange: (event: any) => void, 
    error: boolean,
    errorMessage: string,
    value?: string
    ) : JSX.Element 
{

    if (value == "nil")
        value = "";
        
    return <TextField variant="outlined" fullWidth={true} id={id} value={value} onChange={onChange} error={error} helperText={errorMessage}/>;
}

export function buildCheckbox(
    id: string, 
    onChange: (e: React.ChangeEvent<HTMLInputElement>) => void, 
    value?: boolean
    ) : JSX.Element 
{
    return <Checkbox id={id} onChange={onChange} />;
}

export function buildYesNoDropdown(
    id: string,
    onChange: (id: string, newValue: boolean | null) => void,
    error: boolean,
    errorMessage: string,
    value?: boolean
    ) : JSX.Element
{
    return <YesNoDropdown id={id} value={value} onChange={onChange} error={error} errorMessage={errorMessage}/>
}

export function buildDisplayTypeDropdown(
    id: string,
    onChange: (id: string, newValue: display_type | null) => void,
    error: boolean,
    errorMessage: string,
    value: display_type
    ) : JSX.Element
{
    return <DisplayTypeDropdown id={id} onChange={onChange} error={error} errorMessage={errorMessage} initialValue={value}/>
}

export function buildColorPicker(
    id: string, 
    onChange: (sourceId: string, newColor: string) => void, 
    error: boolean,
    errorMessage: string,
    value?: string,
    ): JSX.Element 
{
    return <ColorPicker color={value || undefined} id={id} onChange={onChange} error={error} errorMessage={errorMessage}/>
}

export function buildFontSelector(
    id: string, 
    onChange: (e: React.ChangeEvent<{
        name?: string | undefined;
        value: unknown;
    }>, child: React.ReactNode) => void,
    error: boolean,
    errorMessage: string,
    value?: string
    ) : JSX.Element 
{
    return <FontSelectorDropdown id={id} onChange={onChange} selectedFont={value} error={error} errorMessage={errorMessage} />
}
