import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

export type TwoOptionDialogProps = {
    onAccept: () => void,
    onCancel: () => void,
    open: boolean,
    primaryButtonText: string,
    dialogTitle: string,
    dialogDescription: string,
}

const TwoOptionDialog:  React.FC<TwoOptionDialogProps> = (props) => {

  return (
    <div>
      <Dialog
        open={props.open}
        onClose={props.onCancel}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{props.dialogTitle}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {props.dialogDescription}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={props.onCancel} color="primary">
            Cancel
          </Button>
          <Button onClick={props.onAccept} color="primary" autoFocus>
            {props.primaryButtonText}
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default TwoOptionDialog;