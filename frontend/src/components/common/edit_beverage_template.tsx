import React from 'react';
import Grid from "@material-ui/core/Grid"
import "../../css/components/edit_beverage_template.css";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import Button from '@material-ui/core/Button';
import InputAdornment from '@material-ui/core/InputAdornment';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/Add';
import { Autocomplete } from '@material-ui/lab';
import AddProducerModal from '../add_producer_modal';
import AddBeverageTypeModal from '../add_beverage_type_modal';
import {PourPriceModal} from '../add-beverage/pour_price_modal';
import PourPriceTable from '../add-beverage/pour_price_table';
import { APICall, Endpoint } from '../../util/apiCall';
import * as InputHelpers from './inputFields';
import ColorPicker from './colorPicker';

type EditBeverageTemplateState = {
  name: string,
  producer: producer | null,
  abv: string,
  color: string,
  type: beverage_type | null,
  description: string,
  producers: producer[],
  beverage_types: beverage_type[],
  errors: {
    name: boolean, producer: boolean, abv: boolean, color: boolean, bev_type: boolean
    description: boolean
  },
  errors_message: {
    name: string, producer: string, abv: string, color: string, bev_type: string,
    description: string
  },
  successfulRequest: boolean,
  showSnackbar: boolean,
  snackbarText: string
  addingPourPrice: boolean,
  pour_prices: pour_price[],
  showCreateProducerModal: boolean,
  showCreateBevTypeModal: boolean,
  editBevID: number, // This parameter is also used to determine if the page is running as create new or edit existing beverage. A -1 means create new, anything else is considered editing.
  constructorFinished: boolean
}

type EditBevProps = {
    bevID: number
}


const defaultState: EditBeverageTemplateState = {
  name: "",
  producer: null,
  abv: "",
  color: "",
  type: null,
  description: "",
  beverage_types: [],
  producers: [],
  addingPourPrice: false,
  pour_prices: [],
  errors: {
    name: false,
    producer: false,
    abv: false,
    color: false,
    description: false,
    bev_type: false
  },
  errors_message: {
    name: "",
    producer: "",
    abv: "",
    color: "",
    description: "",
    bev_type: ""
  },
  successfulRequest: false,
  showSnackbar: false,
  snackbarText: "",
  showCreateBevTypeModal: false,
  showCreateProducerModal: false,
  editBevID: -1, // This parameter is also used to determine if the page is running as create new or edit existing beverage. A -1 means create new, anything else is considered editing.
  constructorFinished: false
}

class EditBeverageTemplate extends React.Component<EditBevProps, EditBeverageTemplateState> {
  pourPriceModal = null;

  constructor(props : EditBevProps) {
    super(props);
    this.state = defaultState;
    if (props.bevID !== -1){
        this.setProducerByID = this.setProducerByID.bind(this);
        this.setTypeByID = this.setTypeByID.bind(this);

        APICall.to(Endpoint.Beverages).getById(props.bevID)
          .then(response => response.json())
          .then(data => {console.log(data);
              this.setProducerByID(data.producer_id);
              this.setTypeByID(data.beverage_type_id);
              this.setState({
                name: data.name,
                abv: "" + data.abv,
                color: data.color,
                description: data.description,
                beverage_types: [],
                producers: [],
                addingPourPrice: false,
                pour_prices: [],
                errors: {
                  name: false,
                  producer: false,
                  abv: false,
                  color: false,
                  description: false,
                  bev_type: false
                },
                errors_message: {
                  name: "",
                  producer: "",
                  abv: "",
                  color: "",
                  description: "",
                  bev_type: ""
                },
                successfulRequest: false,
                showSnackbar: false,
                snackbarText: "",
                showCreateBevTypeModal: false,
                showCreateProducerModal: false,
                editBevID: props.bevID,
                constructorFinished: true
              });
                })
          .catch(() => {this.setState({successfulRequest:false,
                                      showSnackbar: true,
                                      snackbarText: "Failed to save beverage"});
              });

    }
    this.setState({editBevID: props.bevID});
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleProducerChange = this.handleProducerChange.bind(this);
    this.handleABVChange = this.handleABVChange.bind(this);
    this.handleColorChange = this.handleColorChange.bind(this);
    this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
    this.handleTypeChange = this.handleTypeChange.bind(this);
    this.handleAddProducer = this.handleAddProducer.bind(this);
    this.handleSuccessfulProducerCreation = this.handleSuccessfulProducerCreation.bind(this);
    this.handleUnsuccessfulProducerCreation = this.handleUnsuccessfulProducerCreation.bind(this);
    this.handleProducerModalClose = this.handleProducerModalClose.bind(this);
    this.handleAddBevType = this.handleAddBevType.bind(this);
    this.handleSuccessfulBevTypeCreation = this.handleSuccessfulBevTypeCreation.bind(this);
    this.handleUnsuccessfulBevTypeCreation = this.handleUnsuccessfulBevTypeCreation.bind(this);
    this.handleBevTypeModalClose = this.handleBevTypeModalClose.bind(this);

    // Attempt to populate the Beverage Types dropdown with all bevtypes retrieved from the API
    APICall.to(Endpoint.BeverageTypes).get()
      .then(response => response.json())
      .then(data => this.setState({beverage_types: data}))
      .catch(err => this.handleNetworkError("beverage types"));

    // Attempt to populate the Producers dropdown with all producers retrieved from the API
    APICall.to(Endpoint.Producers).get()
      .then(response => response.json())
      .then(data => {
                console.log(data);
                this.setState({producers: data});
            })
      .catch(err => this.handleNetworkError("producers"));
  }


  handleSubmit (event: React.SyntheticEvent) {
    event.preventDefault();

    let name_error = false;
    let producer_error = false;
    let abv_error = null;
    let color_error = null;
    let description_error = false;
    let bev_type_error = false;

    let bev_type_error_msg = '';
    let name_error_msg = "";
    let producer_error_msg = "";
    let description_error_msg = "";
    let color_error_msg = "";


    //Make sure every state component is not blank
    if (this.state.name.trim().length === 0) {
      name_error = true;
      name_error_msg = "Name cannot the empty";}
    if (this.state.producer === null || this.state.producer === undefined) {
      producer_error = true;
      producer_error_msg = "Producer cannot be empty";}
     if (this.state.color === ""){
      color_error = true;
      color_error = true;
      color_error_msg = "* select a color";
 }

    if(this.state.type === null || this.state.producer === undefined){
      bev_type_error = true;
      bev_type_error_msg = "Beverage type cannot be empty";}
    abv_error = this.validate_abv(this.state.abv);
    color_error = this.validate_color(this.state.color);
    if (this.state.description.trim().length === 0) {
      description_error = true;
      description_error_msg = "";}

    if (name_error || producer_error || abv_error.error || color_error.error || description_error || bev_type_error) {
      this.setState({
        errors: {
          name: name_error,
          producer: producer_error,
          abv: abv_error.error,
          color: color_error.error,
          description: description_error,
          bev_type: bev_type_error},
        errors_message: {
          name: name_error_msg,
          producer: producer_error_msg,
          abv: abv_error.message,
          color: color_error.message,
          description: description_error_msg,
          bev_type: bev_type_error_msg},});

    } else {
      var data = new FormData();
      data.append("abv", this.state.abv);
      data.append("name", this.state.name);
      data.append("color", `${this.state.color}`);
      data.append("description", this.state.description);
      data.append("beverage_type", (this.state.type?.type_name as string));
      data.append("producer_id", (this.state.producer?.name as string));
      data.append("pour_prices_attributes", JSON.stringify(this.MapPricesToAttributes(this.state.pour_prices)))

      //Change for production (we don't know what that would look like rn)
      if(this.state.editBevID === -1){
          APICall.to(Endpoint.Beverages).post(data)
          // Successfully saved beverage
          .then(() => {
            //Toast confirming that it worked
            var newState = {
              ...defaultState,
              beverage_types: this.state.beverage_types,
              producers: this.state.producers,
              successfulRequest: true,
              showSnackbar: true,
              snackbarText: "Beverage Saved"
            }
            this.setState(newState, () => console.log("Beverage Submitted"));
          })
          .catch((err) => {
            //Pop up that there was an issue saving;
            this.setState({successfulRequest:false,
                            showSnackbar: true,
                            snackbarText: "Failed to save beverage"});
          });
      }else{
          console.log("Updating beverage");
          if (!process.env.REACT_APP_BACKEND_API)
          throw "Missing backend api url in environment variables";
          const BACKEND_URL = process.env.REACT_APP_BACKEND_API;

          fetch(BACKEND_URL + '/beverages/' + this.state.editBevID, {
              method: 'PATCH',
              body: data
          })
          .then(() => {
              var newState = {
                beverage_types: this.state.beverage_types,
                producers: this.state.producers,
                successfulRequest: true,
                showSnackbar: true,
                snackbarText: "Beverage Saved"
              }
              this.setState(newState, () => console.log("Beverage Submitted"));            })
          .catch((err) => {
              this.setState({successfulRequest:false,
                              showSnackbar: true,
                              snackbarText: "Failed to save beverage"});          });

      }

    }
  }

  MapPricesToAttributes = (prices: pour_price[]): pour_price_attributes[] => {
    prices.forEach(price => {
      if (price.pour_size?.id === -1) {
        price.pour_size_attributes = { size: price.pour_size.size }
      } else {
        price.pour_size_id = price.pour_size?.id
      }
      price.pour_size = undefined
    })
    return prices as pour_price_attributes[]
  }

  setProducerByID = (id: number) => {
      APICall.to(Endpoint.Producers).getById(id)
        .then(response => response.json())
        .then(data => {
            this.setState({producer: data.name});
            })
        .catch(() => {this.setState({successfulRequest:false,
                                    showSnackbar: true,
                                    snackbarText: "Failed to find bev's producer"});
            });
  }

  setTypeByID = (id: number) => {
      APICall.to(Endpoint.BeverageTypes).getById(id)
        .then(response => response.json())
        .then(data => {
            this.setState({type: data.type_name});
            })
        .catch(() => {this.setState({successfulRequest:false,
                                    showSnackbar: true,
                                    snackbarText: "Failed to find bev's type"});
            });
  }


//#region type checking
  validate_abv = (abv: string): validation_error => {
    let wordChars = /[a-zA-Z]/g;
    let abvWordChars = abv.match(wordChars);
    if (abvWordChars != null && abvWordChars.length !== 0) {
      return {error: true, message: "ABV must be of format '#.#', Ex: 4.2"};
    } else if (abv.length === 0) {
      return {error: true, message: "ABV cannot be empty"};
    }
    return {error: false, message: ''};
  }

  validate_color = (colorString: string): validation_error => {
    if (colorString.length === 0) {
      return {error: true, message: "color cannot be empty"};
  }else if (colorString.length !== 6 + 1) {
      return {error: true, message: "color must be a valid hex code"}; }
    return {error: false, message: ''};
  }
//#endregion

//#region handlers

//#region name handlers
  handleNameChange = (e: React.SyntheticEvent) => {
    const name = (e.target as HTMLInputElement).value;
    this.setState({ name: name });
  }
//#endregion

//#region producer handlers
  handleProducerChange = (e: any, producer: producer | null) => {
    this.setState({ producer: producer });
  }

  handleAddProducer = (e: React.SyntheticEvent) => {
    this.setState({showCreateProducerModal: !this.state.showCreateProducerModal});
  }

  handleSuccessfulProducerCreation = (producer: producer) => {
    this.state.producers.push(producer);
    this.setState({
      showCreateProducerModal: false,
      showSnackbar: true,
      snackbarText: "Successfully created producer",
      producers: this.state.producers,
      producer: producer
    });
  }

  handleUnsuccessfulProducerCreation = (e: React.SyntheticEvent) => {
    this.setState({
      showSnackbar: true,
      snackbarText: "Failed to save producer, please make sure there is a name and a description",
      showCreateProducerModal: false,
    });
  }

  handleProducerModalClose = (e: React.SyntheticEvent) => {
    this.setState({showCreateProducerModal: false});
  }
//#endregion

//#region abv handlers
  handleABVChange = (e: React.SyntheticEvent) => {
    const abv = (e.target as HTMLInputElement).value;
    let newABV = abv.replace(/(\s+|%|-)/g, '');
    this.setState({ abv: newABV });
  }
//#endregion

//#region color handlers
handleColorChange = (sourceId: string, newColor: string) => {//e: React.SyntheticEvent) => {
    // const color = (e.target as HTMLInputElement).value;
    // let newColor = color.replace(/[^0-9a-fA-F]/g, '');
    // this.setState({ color: newColor });
    console.log("SourceID: " + sourceId + "  newColor: " + newColor);
    let color = newColor.replace(/[^0-9a-fA-F#]/g, '');
    this.setState({ color: color });
}

//#endregion

//#region beverage type handlers
  handleTypeChange = (e: any, bev_type: beverage_type | null) => {
    this.setState({ type: bev_type });
  }

  handleAddBevType = (e: React.SyntheticEvent) => {
    this.setState({ showCreateBevTypeModal: !this.state.showCreateBevTypeModal });
  }

  handleSuccessfulBevTypeCreation = (bevType: beverage_type) => {
    this.state.beverage_types.push(bevType)
    this.setState({
      showCreateBevTypeModal: false,
      showSnackbar: true,
      snackbarText: "Succesfully created beverage type",
      beverage_types: this.state.beverage_types,
      type: bevType
    });
  }

  handleUnsuccessfulBevTypeCreation = (e: React.SyntheticEvent) => {
    this.setState({
      showSnackbar: true,
      snackbarText: "Failed to save beverage type, please make sure there is a name and a description",
      showCreateBevTypeModal: false
    });
  }

  handleBevTypeModalClose = (e: React.SyntheticEvent) => {
    this.setState({ showCreateBevTypeModal: false });
  }
//#endregion

//#region description handlers
  handleDescriptionChange = (e: React.SyntheticEvent) => {
    const description = (e.target as HTMLInputElement).value;
    this.setState({ description: description });
  }
//#endregion

//#region pour price handlers
  handleCancelPourPrice = () => {
    this.setState({ addingPourPrice: false })
  }

  handleAddPourPrice = (pour_prices: pour_price[]) => {
    this.setState({ pour_prices: pour_prices });
  }
//#endregion

  handleNetworkError = (whatFailedToRetrieve: string) => {
    this.setState({
      showSnackbar: true,
      snackbarText: `There was a network error retrieving ${whatFailedToRetrieve}`
    });
  }
//#endregion

  //TODO: Make so that for desktop centered
  render() {
    return <div >
      <PourPriceModal pourPrices={this.state.pour_prices} open={this.state.addingPourPrice} onCancel={this.handleCancelPourPrice} onSave={this.handleAddPourPrice}></PourPriceModal>
      <AddBeverageTypeModal show={this.state.showCreateBevTypeModal} successfulCreation={this.handleSuccessfulBevTypeCreation} unsuccessfulCreation={this.handleUnsuccessfulBevTypeCreation} cancel={this.handleBevTypeModalClose}/>
      <AddProducerModal show={this.state.showCreateProducerModal} successfulCreation={this.handleSuccessfulProducerCreation} unsuccessfulCreation={this.handleUnsuccessfulProducerCreation} cancel={this.handleProducerModalClose} />
      <form id="bev-form" autoComplete="off" onSubmit={this.handleSubmit}>
        <div>
          <Grid container direction="row" justify="center" alignItems="center">
            <h2>{this.state.editBevID === -1 ? "Creating " : "Editing "}Beverage:</h2>
            <TextField value={this.state.name} error={this.state.errors.name} helperText={this.state.errors_message.name} id="bev-name" label="Beverage Name" onChange={this.handleNameChange} />
          </Grid>
        </div>
        <br />
        <div className="root">
          <Grid container direction="column" justify="center" alignItems="flex-start">

            <Grid container direction="row" justify="space-between" alignItems="center">
                  <h3>Producer:</h3>
                  <span className={'input-field-area'}>
                    <Autocomplete
                        openOnFocus
                        autoComplete
                        className={'input-field-with-button'}
                        id="bev-producer"
                        options={this.state.producers}
                        getOptionLabel={option => option.name}
                        value={this.state.producer}
                        onChange={this.handleProducerChange}
                        renderInput={params => <TextField {...params} label="Producer" variant="outlined" error={this.state.errors.producer} helperText={this.state.errors_message.producer}/>}
                        />
                    <AddIcon id="add-producer-icon" onClick={this.handleAddProducer}/>
                  </span>
            </Grid>
            <br/>

            <Grid container direction="row" justify="space-between" alignItems="center">
              <h3>ABV (%):</h3>
              <TextField className={'input-field'} value={this.state.abv} error={this.state.errors.abv} helperText={this.state.errors_message.abv}
                InputProps={{
                  endAdornment: <InputAdornment position="end">%</InputAdornment>,
                }}
                id="bev-abv" label="Ex: 4.2" variant="outlined" onChange={this.handleABVChange} />
            </Grid>
            <br />

            <Grid container direction="row" justify="space-between" alignItems="center">
            <h3>Color:</h3>
            <div>
            {
              this.state.constructorFinished 
                ? <ColorPicker color={this.state.color} onChange={this.handleColorChange} error={this.state.errors.color} errorMessage={this.state.errors_message.color}/> 
                : <div/>
            }
            { 
              this.state.editBevID === -1 
                ? <ColorPicker onChange={this.handleColorChange} error={this.state.errors.color} errorMessage={this.state.errors_message.color} /> 
                : <div/>
            }
            </div>
            </Grid>
            <br />

            <Grid container direction="row" justify="space-between" alignItems="center">
              <h3>Type:</h3>
              <span className={'input-field-area'}>
                <Autocomplete className={'input-field-with-button'}
                  openOnFocus
                  autoComplete
                  id="bev-type"
                  value={this.state.type}
                  options={this.state.beverage_types}
                  getOptionLabel={option => option.type_name}
                  onChange={this.handleTypeChange}
                  renderInput={params => <TextField {...params} label="Beverage Type" variant="outlined" error={this.state.errors.bev_type} helperText={this.state.errors_message.bev_type}/>}
                  />
                <AddIcon id="add-bev-type-icon" onClick={this.handleAddBevType} />
              </span>
            </Grid>
            <br />

            <Grid container direction="row" justify="space-between" alignItems="center">
              <h3>Pour Prices:</h3>
              <Button variant="outlined" onClick={() => { this.setState({ addingPourPrice: true }) }}>Edit</Button>
            </Grid>
            <br />

            <Grid container direction="row" alignItems="flex-start" justify="flex-end">
              <PourPriceTable showIfEmpty={false} pourPrices={this.state.pour_prices} allowRemove={false} maxWidth={250}>
              </PourPriceTable>
            </Grid>
            <br />

            <Grid container direction="column">
              <TextField value={this.state.description} error={this.state.errors.description} helperText={this.state.errors_message.description} onChange={this.handleDescriptionChange} id="bev-description" label="Description" variant="outlined" placeholder="Description" />
            </Grid>
            <Grid container direction="row" justify="flex-end" alignItems="center">
              <Button id="cancel-button" variant="outlined" href="/">Cancel</Button>
              <Button id="save-button" variant="contained" color="primary" type="submit">Save</Button>
            </Grid>
          </Grid>

        </div>
      </form>
      <Snackbar
        open={this.state.showSnackbar}
        autoHideDuration={6000}
        onClose={(e) => this.setState({ showSnackbar: false })}
        message={this.state.snackbarText}
        action={
          <React.Fragment>
            <IconButton size="small" aria-label="close" color="inherit" onClick={(e) => this.setState({ showSnackbar: false })}>
            </IconButton>
          </React.Fragment>
        }
      />
    </div>
  }
}
export default EditBeverageTemplate;
