import React from 'react';
import * as ColorPickers from 'react-color';
import TextField from "@material-ui/core/TextField";
import { InputAdornment, withStyles, Theme, createStyles, createMuiTheme, ThemeProvider, Grid, DialogActions, DialogContent, DialogTitle, Badge, FormHelperText } from '@material-ui/core';
import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import TwoOptionDialog from './two_option_dialog';
import "../../css/components/common/color_picker.css";

type ColorPickerButtonProps = {
    color?: string;
    onChange: (sourceId: string, newColor: string) => void;
    id?: string;
    error: boolean;
    errorMessage: string;
}

type ColorPickerButtonState = {
    theme: Theme;
    color?: string;
    tempColor: string;
    showColorPickerPopup: boolean;
}

const defaultColor = "#a8a8a8";

class ColorPickerButton extends React.Component<ColorPickerButtonProps, ColorPickerButtonState> {

    constructor(props: ColorPickerButtonProps) {
        super(props);
        this.state = {
            showColorPickerPopup: false,
            color: this.props.color,
            tempColor: this.props.color || defaultColor, 
            theme: createMuiTheme({
                palette: {
                    primary: {
                        main: this.props.color || defaultColor
                    }
                }
            })
        }

        this.saveNewColor = this.saveNewColor.bind(this);
        this.openColorPickerPopup = this.openColorPickerPopup.bind(this);
        this.cancelNewColor = this.cancelNewColor.bind(this);
        this.onColorChanged = this.onColorChanged.bind(this);
    }

    onColorChanged(color: ColorPickers.ColorResult) {
        this.setState({
            color: color.hex,
            theme: createMuiTheme({
                palette: {
                    primary: {
                        main: color.hex
                    }
                }
            })
        });
    }

    saveNewColor() {
        this.setState({
            showColorPickerPopup: false,
        });

        this.props.onChange(this.props.id || "no-id-assigned", this.state.color as string);
    }

    cancelNewColor() {
        this.setState({
            showColorPickerPopup: false,
            color: this.state.tempColor
        });
    }

    openColorPickerPopup() {
        this.setState({
            showColorPickerPopup: true,
            tempColor: this.state.color || defaultColor
        });
    }

    render() {

        return <ThemeProvider theme={this.state.theme}> 
            
            <Badge color="error" invisible={!this.props.error} badgeContent={"!"} style={{width: '100%'}}>
                <Button 
                    fullWidth={true} 
                    variant="contained" 
                    color="primary" 
                    onClick={this.openColorPickerPopup} 
                    id={this.props.id}
                    style={{width: '100%', maxWidth: '250px'}}>
                    {this.state && this.state.color ? this.state.color : "Click to select"}
                </Button>
            </Badge>
            <FormHelperText error>{this.props.errorMessage}</FormHelperText>
            
            <Dialog open={this.state.showColorPickerPopup} style={{boxShadow: "none !important"}}>
                
                <DialogTitle>
                    Select a color
                </DialogTitle>

                <DialogContent>
                    <ColorPickers.ChromePicker 
                        color={this.state.color}
                        onChange={this.onColorChanged}
                    />
                </DialogContent>

                <DialogActions>
                    <Button variant="outlined" color="primary" onClick={this.cancelNewColor}>Cancel</Button>
                    <Button variant="contained" color="primary" onClick={this.saveNewColor}>Done</Button>
                </DialogActions>

            </Dialog>
        </ThemeProvider>
    }
}

export default ColorPickerButton;