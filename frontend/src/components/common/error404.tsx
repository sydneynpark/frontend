import React from 'react';
import GlobalThemeProvider from './themeProvider';
import '../../css/pages/page.css';


type Error404Props = {
    message?: string
}

const Error404: React.FC<Error404Props> = (props: Error404Props) => {
    return <div className="page">
        <GlobalThemeProvider>
            <h2 color="primary">Error 404: Not Found</h2>
            <p>
                {
                    props.message 
                    ||
                    "The item you are attempting to access does not exist."
                }
            </p>
        </GlobalThemeProvider>
    </div>
}

export default Error404;