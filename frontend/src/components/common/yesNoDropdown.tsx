import { Select, MenuItem, FormHelperText } from "@material-ui/core"
import React from "react";

export type YesNoProps = {
    id: string;
    value?: boolean;
    onChange: (id: string, newValue: boolean | null) => void;
    error: boolean;
    errorMessage: string;
}

export type YesNoState = {
    value: "Y" | "N" | "-";
}

class YesNoDropdown extends React.Component<YesNoProps, YesNoState> {
    constructor(props: YesNoProps) {
        super(props);
        if (this.props.value === undefined) {
            this.state = { 
                value: '-'
            };
        } else {
            this.state = {
                value: this.props.value == true ? "Y" : "N"
            };
        }

        this.handleDropdownChanged = this.handleDropdownChanged.bind(this);
    }

    private handleDropdownChanged(e: React.ChangeEvent<{
        name?: string | undefined;
        value: unknown;
    }>, child: React.ReactNode) {
        let inputElement = (e.target as HTMLInputElement);
        
        let newValue = null;
        let newStateValue: "Y"|"N"|"-" = "-";

        if (inputElement.value == "Y") {
            newValue = true;
            newStateValue = "Y";
        }
        else if (inputElement.value == "N") {
            newValue = false;
            newStateValue = "N";
        }

        this.setState({ value: newStateValue });
        
        this.props.onChange(inputElement.name, newValue);
    }


    render() {
        return <React.Fragment>
            <Select
                variant="outlined"
                fullWidth={true}
                name={this.props.id}
                onChange={this.handleDropdownChanged}
                error={this.props.error}
                value={this.state.value}
            >
                <MenuItem key={"undefined"} value="-">-</MenuItem>
                <MenuItem key="Y" value="Y">Yes</MenuItem>
                <MenuItem key="N" value="N">No</MenuItem>
            </Select>
            <FormHelperText error>{this.props.errorMessage}</FormHelperText>
        </React.Fragment>
    }
}

export default YesNoDropdown;