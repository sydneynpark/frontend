import React from 'react';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import AddTaproomModal from '../../components/add_taproom_modal';
import { Redirect } from 'react-router-dom'
import NewTaproomPage from '../../pages/new_taproom'

export default function NavbarMenu() {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  var redirect = false;

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleCreateTaproom = () => {
    setAnchorEl(null);
    redirect = true;
  };

  const renderRedirect = () => {
    handleCreateTaproom();
    if(redirect) {
      window.location.pathname = '/newTaproom';
    }
  };

  const redirectToDefaultMenu = () => {
    setAnchorEl(null);
    window.location.pathname = '/menu/1'
  }

  const redirectToTaprooms = () => {
    setAnchorEl(null);
    window.location.pathname = '/taprooms'
  }    
  const redirectToMenuStylesList = () => {
    window.location.pathname = '/menuStyles'
  }
  const redirectToBackup = () => {
    window.location.pathname = '/backup'
  }

  const redirectToUpload = () => {
    window.location.pathname = '/upload'
  }


  return (
    <div>
      <IconButton edge="start" aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick} color="inherit">
        <MenuIcon />
      </IconButton>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem onClick={renderRedirect}>Add Taproom</MenuItem>
        <MenuItem onClick={redirectToDefaultMenu}>Default Menu</MenuItem>
        <MenuItem onClick={redirectToTaprooms}>Taprooms</MenuItem>
        <MenuItem onClick={redirectToMenuStylesList}>Menu Styles</MenuItem>
        <MenuItem onClick={redirectToBackup}>Backup Your Data</MenuItem>
        <MenuItem onClick={redirectToUpload}>Upload Your Backup</MenuItem>
      </Menu>
    </div>
  );
}
