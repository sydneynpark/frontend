import React from 'react';
import { MenuItem, Select, FormHelperText } from '@material-ui/core';
import "../../css/components/common/font_selector.css"

type FontSelectorProps = {
    selectedFont?: string;
    id: string;
    onChange: (event: React.ChangeEvent<{
            name?: string | undefined;
            value: unknown;
        }>, child: React.ReactNode) => void;
    error: boolean;
    errorMessage: string;
}

export const FONT_DISPLAY_TO_STORAGE_NAMES: Map<string, string> = new Map([
    ["-", ""],
    ["Arial", "arial"],
    ["Arial Black", "arial-black"],
    ["Comic Sans", "comic-sans"],
    ["Courier", "courier"],
    ["Georgia", "georgia"],
    ["Impact", "impact"],
    ["Lucida Console", "lucida-console"],
    ["Lucida Sans", "lucida-sans"],
    ["Palatino", "palatino"],
    ["Sans Serif", "sans-serif"],
    ["Stentiga", "stentiga"],
    ["Tahoma", "tahoma"],
    ["Times New Roman", "times"],
    ["Trebuchet", "trebuchet"],
    ["Verdana", "verdana"],
]);

export const STORAGE_NAME_TO_FONT_FAMILY: Map<string, string> = new Map([
    ["arial", 'Arial, Helvetica, sans-serif'],
    ["arial-black", '"Arial Black", Gadget, sans-serif'],
    ["comic-sans", '"Comic Sans MS", cursive, sans-serif'],
    ["courier", '"Courier New", Courier, monospace'],
    ["georgia", 'Georgia, serif'],
    ["impact", 'Impact, Charcoal, sans-serif'],
    ["lucida-console", '"Lucida Console", Monaco, monospace'],
    ["lucida-sans", '"Lucida Sans Unicode", "Lucida Grande", sans-serif'],
    ["palatino", '"Palatino Linotype", "Book Antiqua", Palatino, serif'],
    ["sans-serif", 'sans-serif'],
    ["stentiga", 'stentiga'],
    ["tahoma", 'Tahoma, Geneva, sans-serif'],
    ["times", '"Times New Roman", Times, serif'],
    ["trebuchet", '"Trebuchet MS", Helvetica, sans-serif'],
    ["verdana", 'Verdana, Geneva, sans-serif'],
]);


class FontSelectorDropdown extends React.Component<any, FontSelectorProps> {

    constructor(props: FontSelectorProps) {
        super(props);

        this.state = {
            selectedFont: this.props.selectedFont || "",
            id: this.props.id,
            onChange: this.props.onChange,
            error: false,
            errorMessage: this.props.errorMessage,
        };

        this.handleSelectionChanged = this.handleSelectionChanged.bind(this);
    }

    handleSelectionChanged(event: React.ChangeEvent<{
        name?: string | undefined;
        value: unknown;
    }>, child: React.ReactNode) {
        this.setState({
            selectedFont: (event.target.value as string)
        });

        this.state.onChange(event, child);
    }

    buildMenuItems(): JSX.Element[] {
        let fontOptions: JSX.Element[] = [];
        FONT_DISPLAY_TO_STORAGE_NAMES.forEach((storageName: string, displayName: string) => {
            fontOptions.push(<MenuItem key={storageName} value={storageName}>{displayName}</MenuItem>)
        });

        return fontOptions;
    }

    render() {
    
        return <React.Fragment>
            <Select
                variant="outlined"
                fullWidth={true}
                value={this.state.selectedFont}
                onChange={this.handleSelectionChanged}
                error={this.props.error}
                defaultValue={""}
                style={{width: '100%'}}
                id={this.state.id}
                name={this.state.id}
                className="font-selector"
            >
                { this.buildMenuItems() }
            </Select>
            <FormHelperText error>{this.props.errorMessage}</FormHelperText>
        </React.Fragment>

    }

}

export default FontSelectorDropdown;