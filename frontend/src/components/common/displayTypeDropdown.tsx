import { Select, MenuItem, FormHelperText } from "@material-ui/core"
import React, { useEffect } from "react";

export type DisplayTypeDropdownProps = {
    id: string,
    onChange: (id: string, newValue: display_type | null) => void;
    error: boolean;
    errorMessage: string;
    initialValue: display_type;
}

const DisplayTypeDropdown: React.FC<DisplayTypeDropdownProps> = (props: DisplayTypeDropdownProps) => {

    const handleDropdownChanged = (e: any) => {
        let inputElement = (e.target as HTMLInputElement);

        let newValue: display_type = inputElement.value as display_type;

        props.onChange(inputElement.name, newValue);
    }


    return (
        <React.Fragment>
            <Select
                variant="outlined"
                fullWidth={true}
                name={props.id}
                onChange={handleDropdownChanged}
                error={props.error}
                value={props.initialValue}
            >
                <MenuItem key="card" value="card">Card View</MenuItem>
                <MenuItem key="grid" value="grid">Table View</MenuItem>
            </Select>
            <FormHelperText error>{props.errorMessage}</FormHelperText>
        </React.Fragment>
    )
}

export default DisplayTypeDropdown;