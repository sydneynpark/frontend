import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import logo from "../../assets/rnogginLogo.png";
import "../../css/components/navbar.css";
import {isMobile} from 'react-device-detect';
import NavbarMenu from  './navbar_menu';

let titleText = "R'Noggin Brewing Co.";
if (isMobile) {
  titleText = "";
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 0,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
  }),
);

export default function MenuAppBar() {
  const classes = useStyles();
  const [auth, setAuth] = React.useState(true);
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);

  const handleMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
    sessionStorage.removeItem('user');
    sessionStorage.isAuthenticated = false;
    window.location.reload();
  };

  const getUserName = () => {
    var userSessionString = sessionStorage.getItem('user');
    if(userSessionString){
      var user = JSON.parse(userSessionString);
      return user.firstName + " " + user.lastName;
    }
    return "";
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static" id="appBar">
        <Toolbar>
          <NavbarMenu/>
          <Typography variant="h6" className={classes.title}>
            <div id="logoBar">
              <a href="/" style={{textDecoration: 'none'}}>
                <img id="logoImg" alt="Logo" src={logo} />
                <span id="logoText"> <b>{titleText}</b> </span>
              </a>
            </div>
          </Typography>
          {auth && (
            <div id={'userInfoSection'}>
              <Typography variant="h6" className={classes.title}>
                <span id="userName"> <b>{getUserName()}</b> </span>
              </Typography>
              <IconButton
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={handleMenu}
                color="inherit"
              >
                <AccountCircle />
              </IconButton>
              <Menu
                id="menu-appbar"
                anchorEl={anchorEl}
                anchorOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                keepMounted
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                open={open}
                onClose={handleClose}
              >
                <MenuItem onClick={handleClose}>Admin</MenuItem>
                <MenuItem onClick={handleClose}>Logout</MenuItem>
              </Menu>
            </div>
          )}
        </Toolbar>
      </AppBar>
    </div>
  );
}
