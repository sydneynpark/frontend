import React from 'react';
import { ThemeProvider, createMuiTheme } from '@material-ui/core';


const GlobalThemeProvider: React.FC = (props) => {
    return <ThemeProvider theme={createMuiTheme({
        palette: {
            primary: {
                main: "#008000",
                dark: "#006600",
                light: "#329932"
            },
            secondary: {
                main: "#a8a8a8",
                dark: "#868686",
                light: "#b9b9b9"
            }
        }
    })}>
        {props.children}
    </ThemeProvider>
}

export default GlobalThemeProvider;