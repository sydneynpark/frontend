import React from 'react';
import { GoogleLogin } from 'react-google-login';
import EnvironmentManager from '../environment/EnvironmentManager';
import '../../css/components/login/login_card.css';
import ChangeIfMobile from '../../css/helpers'
import TwoOptionDialog from '../common/two_option_dialog'
import Alert from '../common/alert'
import { useEffect } from 'react';
import { APICall, Endpoint } from '../../util/apiCall';

type LoginCardState = {
    noExistingAccount: boolean,
    accountAlreadyExists: boolean,
    googleError: boolean,
    errorMessage: string,
    errorTitle: string,
    code: string
    response: any
    responseBody: any
}

let defaultState: LoginCardState = {
    noExistingAccount: false,
    accountAlreadyExists: false,
    googleError: false,
    errorMessage: '',
    errorTitle: '',
    code: '',
    response: null,
    responseBody: null,
};

const LoginCard: React.FC = () => {
    const [state, setState] = React.useState(defaultState);
    let createButton: HTMLButtonElement | null;

    async function loginUser(loginResponse: any) {
        sessionStorage.setItem('user', JSON.stringify({
            firstName: loginResponse.given_name,
            lastName: loginResponse.family_name,
            email: loginResponse.email,
            id: loginResponse.sub
        }));
        sessionStorage.isAuthenticated = true;
        window.location.reload();
    }

    useEffect(() => {
        if (state.responseBody != null) {
            loginUser(state.responseBody)
        }
    }, [state.responseBody])

    async function Login(googleResponse: any, create: boolean, login: boolean) {
        //create a request from the offline code
        let tokenBlob = new Blob([JSON.stringify({ access_token: googleResponse.code }, null, 2)], { type: 'application/json' });
        let options: RequestInit = {
            mode: 'cors',
            cache: 'default'
        };

        //send the login or create new account request
        var subUrl = 'create';
        if (login) {
            subUrl = 'login';
        }

        APICall.to(Endpoint.Users, subUrl)
        .suppressError() // we want to perform custom handling of the error code
        .withOptions(options) // send the fetch request with the options created
        .post(tokenBlob)
        .then(response => {

            //if the response is good then login the user
            if (response.status == 200) {
                response.json().then(body => setState({ ...state, responseBody: body }));

                //if the response statis is 403, then an account already exists or no account exists depending on the action
            } else if (response.status == 403) {
                setState({
                    ...state,
                    noExistingAccount: login,
                    accountAlreadyExists: create,
                    code: googleResponse.code,
                    response: response
                })

                //if the response is any other status then an unknown error occured
            } else {
                onFailure({ error: "Connection Error", description: "An unknown error occurred in communication with the API." })
            }
        }).catch(error => {
            onFailure({ error: "Connection Error", description: "An unknown error occurred in communication with the API." });
        });
    };

    function onFailure(response: any) {
        let message: string;
        switch (response.error) {
            case "idpiframe_initialization_failed":
                message = "Initialization of the Google Auth API failed, try enabling third party cookies."
                break;
            case "popup_closed_by_user":
            case "access_denied":
            case "immediate_failed":
                return
            case "Connection Error":
                message = response.description;
                break;
            default:
                message = "An unknown error occured while loggin in with google, please try again."
        }
        setState({
            ...state,
            googleError: true,
            errorTitle: response.error,
            errorMessage: message,
            code: ''
        })
    }

    return (
        <div className={ChangeIfMobile('login-card')}>
            <TwoOptionDialog
                onAccept={() => {
                    createButton?.click();
                    setState({ ...state, noExistingAccount: false, code: '' })
                }}
                onCancel={() => { setState({ ...state, noExistingAccount: false }) }}
                dialogDescription={"Would you like to create a new account?"}
                dialogTitle={"Account Doesn't Exist"}
                open={state.noExistingAccount}
                primaryButtonText={"Create New"}
            />
            <TwoOptionDialog
                onAccept={() => {
                    setState({ ...state, accountAlreadyExists: false });
                    state.response.json().then((body: any) => loginUser(body));
                }}
                onCancel={() => { setState({ ...state, accountAlreadyExists: false }) }}
                dialogDescription={"Would you like to login?"}
                dialogTitle={"Account Already Exists"}
                open={state.accountAlreadyExists}
                primaryButtonText={"Login"}
            />
            <Alert
                dialogTitle={state.errorTitle}
                dialogDescription={state.errorMessage}
                buttonText={"OK"}
                open={state.googleError}
                onAccept={() => {
                    setState({ ...state, googleError: false, errorMessage: '' })
                }}
            />
            <div className={'card-content'}>
                <h1 className={ChangeIfMobile('title')}>AHOPS Brewery Menu</h1>
                <h1 className={ChangeIfMobile('greeting')}>Welcome! Please Login.</h1>
                <GoogleLogin
                    className={'login-button'}
                    clientId={EnvironmentManager.retrieveGoogleKey()}
                    render={renderProps => (
                        <button
                            onClick={renderProps.onClick}
                            disabled={renderProps.disabled}
                            className={'google-button'}
                        >Login With Google
                        </button>
                    )}
                    onSuccess={(response) => Login(response, false, true)}
                    onFailure={onFailure}
                    responseType={'code'}
                />
                <GoogleLogin
                    className={'create-button'}
                    clientId={EnvironmentManager.retrieveGoogleKey()}
                    render={renderProps => (
                        <button
                            ref={elem => createButton = elem}
                            onClick={renderProps.onClick}
                            disabled={renderProps.disabled}
                            className={'google-button'}
                        >Create Account
                        </button>
                    )}
                    onSuccess={(response) => Login(response, true, false)}
                    onFailure={onFailure}
                    responseType={'code'}
                    accessType={'offline'}
                    prompt={'consent'}
                />
            </div>
        </div>
    );

}

export default LoginCard