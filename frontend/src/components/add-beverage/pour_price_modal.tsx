import React, { useEffect } from 'react';
import { Dialog, Snackbar, IconButton } from '@material-ui/core'
import { DialogTitle } from '@material-ui/core'
import { DialogContent } from '@material-ui/core'
import { DialogActions } from '@material-ui/core'
import { DialogContentText } from '@material-ui/core'
import { Button } from '@material-ui/core'
import { TextField } from '@material-ui/core'
import { TableCell } from '@material-ui/core';
import { TableRow } from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import { InputAdornment } from '@material-ui/core';
import '../../css/beverage/pour_price_modal.css';
import PourPriceTable from './pour_price_table';

//#region PourPriceModalState
type PourPriceModalState = {
    pour_prices_available: pour_price[],
    pour_sizes_available: pour_size[],
    pour_prices_selected: pour_price[],
    error: boolean,
    errorMessage: string
}
const defaultState: PourPriceModalState = {
    pour_prices_available: [],
    pour_sizes_available: [],
    pour_prices_selected: [],
    error: false,
    errorMessage: '',
}
//#endregion

//#region PourPriceValuesState
type CreatePourValuesState = {
    pour_price: string | null,
    pour_size: string | null
}
const defaultValues: CreatePourValuesState = {
    pour_price: null,
    pour_size: null
}
//#endregion

//#region InputFieldErrorsState
type InputFieldErrorsState = {
    pour_price_error: boolean,
    pour_price_error_text: string,
    pour_size_error: boolean,
    pour_size_error_text: string
}
const defaultErrors: InputFieldErrorsState = {
    pour_price_error: false,
    pour_price_error_text: 'enter a number',
    pour_size_error: false,
    pour_size_error_text: 'enter text'
}
//#endregion

//#region selection parsing types
type PourSizeSelectionResponse = {
    pourSize: pour_size | undefined,
    errors: Partial<InputFieldErrorsState>,
}
type PourPriceSelectionResponse = {
    pourPrice: pour_price | undefined,
    errors: Partial<InputFieldErrorsState>,
}
//#endregion

export type PourPriceModalProps = {
    open: boolean
    onCancel: () => void,
    onSave: (pour_prices: pour_price[]) => void
    pourPrices: pour_price[]
}

const formatter = new Intl.NumberFormat('en-US',
    {
        minimumFractionDigits: 2
    });

export const PourPriceModal: React.FC<PourPriceModalProps> = (props: PourPriceModalProps) => {
    const [state, setState] = React.useState<PourPriceModalState>({...defaultState, pour_prices_selected: [...props.pourPrices]});
    const [values, setValues] = React.useState<CreatePourValuesState>(defaultValues);
    const [errors, setErrors] = React.useState<InputFieldErrorsState>(defaultErrors);

    useEffect(() => {
        fetchData();
    }, []);

    const fetchData = async () => {
        var newState: Partial<PourPriceModalState> = {};

        //get the pour prices
        try {
            var pricesResponse = await fetch(process.env.REACT_APP_BACKEND_API + '/pour_prices/prices', { method: 'GET' })
            if (pricesResponse.ok) {
                var responseBody = await pricesResponse.json()
                //map the return prices to pour_price types
                newState = { pour_prices_available: (responseBody as number[]).map(price => { return { pour_price: price } }) }
            } else {
                newState = { ...newState, error: true, errorMessage: "There was an internal error with the API." }
            }
        } catch{
            newState = { ...newState, error: true, errorMessage: "There was a network error retrieving pour prices" }
        }

        //get the pour sizes
        try {
            var sizesResponse = await fetch(process.env.REACT_APP_BACKEND_API + '/pour_sizes', { method: 'GET' })
            if (sizesResponse.ok) {
                var responseBody = await sizesResponse.json()
                newState = { ...newState, pour_sizes_available: responseBody };
            } else {
                newState = { ...newState, error: true, errorMessage: "There was an internal error with the API." }
            }
        } catch{
            newState = { ...newState, error: true, errorMessage: "There was a network error retrieving pour sizes" }
        }

        setState({ ...state, ...newState })
    }

    useEffect(() => {
        if(props.pourPrices != state.pour_prices_selected){
            setState({...state, pour_prices_selected: [...props.pourPrices]});
            console.log("reset pourPrices from beverage page");
        }
    }, [props.pourPrices])

    const handleClose = () => {
        props.onCancel();
        setValues({ pour_price: '', pour_size: null });
    }

    const handleSave = () => {
        props.onSave(state.pour_prices_selected);
        handleClose();
    }

    const handleAddPourPrice = () => {
        var pourPriceResponse: PourPriceSelectionResponse = getPourPriceFromSelection();
        var pourSizeResponse: PourSizeSelectionResponse =getPourSizeFromSelection();

        if (pourSizeResponse.pourSize != undefined && pourPriceResponse.pourPrice != undefined) {
            pourPriceResponse.pourPrice.pour_size = pourSizeResponse.pourSize;
            state.pour_prices_selected.push(pourPriceResponse.pourPrice);
            setState({ ...state });
            setValues({ pour_price: '', pour_size: null })
            setErrors(defaultErrors);
        } else {
            setErrors({ ...errors, ...pourPriceResponse.errors, ...pourSizeResponse.errors })
        }
    }

    const getPourPriceFromSelection = (): PourPriceSelectionResponse => {
        if (values.pour_price != null && values.pour_price.trim() != '') {
            var price = parseInt(values.pour_price);

            if (isNaN(price)) {
                return { pourPrice: undefined, errors: { pour_price_error: true, pour_price_error_text: 'invalid price' } }
            } else if (price < 0) {
                return { pourPrice: undefined, errors: { pour_price_error: true, pour_price_error_text: 'negative price?' } }
            } else if (price > 1000000) {
                return { pourPrice: undefined, errors: { pour_price_error: true, pour_price_error_text: 'are you serious?' } }
            }

            var pourPrice = state.pour_prices_available.find(item => item.pour_price === price);
            if (pourPrice === undefined) {
                pourPrice = {
                    pour_price: price
                }
            }
            //{...pourPrice} creates a simple copy of the pour price object to return
            return { pourPrice: {...pourPrice}, errors: {} };
        }
        return { pourPrice: undefined, errors: { pour_price_error: true, pour_price_error_text: 'missing price' } }
    }

    const getPourSizeFromSelection = (): PourSizeSelectionResponse => {
        if (values.pour_size != null && values.pour_size.trim() != '') {
            var size = values.pour_size.trim();

            if(state.pour_prices_selected.find(price => price.pour_size?.size === size))
                return{pourSize: undefined, errors: {pour_size_error:  true, pour_size_error_text: 'size already exists'}};

            var pourSize = state.pour_sizes_available.find(item => item.size === size);
            if (pourSize === undefined) {
                pourSize = {
                    id: -1,
                    size: size
                }
            }
            //{...pourSize} creates a simple copy of the pour size object to return
            return { pourSize: {...pourSize}, errors: {} };
        }
        return { pourSize: undefined, errors: { pour_size_error: true, pour_size_error_text: 'missing size' } }
    }

    return (
        <div>
            <Dialog className={'pour-price-modal'} open={props.open && !state.error} onClose={handleClose} id={"pour-price-modal"}>
                <DialogTitle id="form-dialog-title">Edit Pour Prices</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Add, remove, and edit the pour prices for this beverage. the pour sizes are unique values and cannot be duplicated.
                    </DialogContentText>
                    <PourPriceTable allowRemove={true}
                        onRemove={(index) => {
                            state.pour_prices_selected.splice(index, 1);
                            setState({ ...state })
                        }}
                        showIfEmpty={true}
                        pourPrices={state.pour_prices_selected}>
                        <TableRow key={1}>
                            <TableCell align="center">
                                <Autocomplete id={"pour-size-selector"}
                                    freeSolo
                                    autoSelect
                                    className={'pour-size-selector'}
                                    options={state.pour_sizes_available.map(pour_size => pour_size.size)}
                                    value={values.pour_size}
                                    onChange={(_event: any, value: string | null) => 
                                        setValues({ ...values, pour_size: value })
                                    }
                                    onInputChange={() => {
                                        if (errors.pour_size_error)
                                            setErrors({ ...errors, pour_size_error: false, pour_size_error_text: 'enter text' })
                                    }}
                                    renderInput={params => (
                                        <TextField {...params} label="Pour Size" margin="normal" variant="outlined" fullWidth
                                            error={errors.pour_size_error}
                                            helperText={errors.pour_size_error_text} 
                                            inputProps={{...params.inputProps, maxlength: 10}}/>
                                    )} />
                            </TableCell>
                            <TableCell component="th" scope="row" align="center">
                                <Autocomplete id={"pour-price-selector"}
                                    className={'pour-price-selector'}
                                    freeSolo
                                    autoSelect
                                    options={state.pour_prices_available.map(price => formatter.format(price.pour_price))}
                                    value={values.pour_price}
                                    onChange={(event: any, value: string | null) => {
                                        setValues({ ...values, pour_price: value })
                                    }}
                                    onInputChange={() => {
                                        if (errors.pour_price_error)
                                            setErrors({ ...errors, pour_price_error: false, pour_price_error_text: 'enter a number' })
                                    }}
                                    renderInput={params => (
                                        <TextField {...params} label="Pour Price" margin="normal" variant="outlined" type="number" fullWidth
                                            error={errors.pour_price_error}
                                            helperText={errors.pour_price_error_text}
                                            InputProps={{...params.InputProps, startAdornment: <InputAdornment position="start">$</InputAdornment>}}/>
                                    )} />
                            </TableCell>
                            <TableCell align="right">
                                <Button onClick={handleAddPourPrice} variant='contained' color='primary'  id={'add-button'} className={'add-button'}>Add</Button>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell align="left" id={'mobile-add-button'} className={'mobile-add-button'}>
                                <Button onClick={handleAddPourPrice} variant='contained' color='primary'>Add</Button>
                            </TableCell>
                        </TableRow>
                    </PourPriceTable>
                    <DialogActions>
                        <Button onClick={handleClose} color="primary" variant='outlined' id={"cancel-button"}>
                            Cancel
                    </Button>
                        <Button onClick={handleSave} color="primary" variant='contained' style={{ marginRight: '6px' }} id={"save-pour-prices-button"}>
                            Save
                    </Button>
                    </DialogActions>
                </DialogContent>
            </Dialog>
            <Snackbar id={"pour-price-error-toast"}
                open={state.error && props.open}
                autoHideDuration={6000}
                onClose={(e) => {
                    props.onCancel();
                }}
                message={state.errorMessage}
                action={
                    <React.Fragment>
                        <IconButton size="small" aria-label="close" color="inherit" onClick={(e) => {
                            props.onCancel();
                        }}>
                        </IconButton>
                    </React.Fragment>
                }
            />
        </div>);


}

export default {PourPriceModal};