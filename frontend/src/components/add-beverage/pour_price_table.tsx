import React from 'react';
import { Table } from '@material-ui/core';
import { TableBody } from '@material-ui/core';
import { TableCell } from '@material-ui/core';
import { TableContainer } from '@material-ui/core';
import { TableHead } from '@material-ui/core';
import { TableRow } from '@material-ui/core';
import { IconButton } from '@material-ui/core';
import { Delete } from '@material-ui/icons';
import '../../css/beverage/pour_price_modal.css'

type PourPriceTable = {
    showIfEmpty: boolean,
    allowRemove: boolean,
    onRemove?: (idex: number) => void,
    pourPrices: pour_price[]
    maxWidth?: number,
    id?: string
}

var formatter = new Intl.NumberFormat('en-US',
    {
        minimumFractionDigits: 2
    });

const PourPriceTable: React.FC<PourPriceTable> = (props) => {

    return (
        <React.Fragment>
            {(props.showIfEmpty || (props.pourPrices.length > 0)) &&
                <TableContainer style={props.maxWidth != null ? {maxWidth: `${props.maxWidth}px`} : {}}>
                    <Table aria-label="pour price table" size="small" id={"pour-price-table"}>
                        <TableHead>
                            <TableRow style={{backgroundColor: 'darkGray'}}>
                                <TableCell align="center">Pour Size</TableCell>
                                <TableCell align="center">Pour Price</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {props.pourPrices.map((pourPrice, index) =>
                                <TableRow key={pourPrice.pour_size?.size} >
                                    <TableCell 
                                        align="center" 
                                        style={(index%2 != 0) ? {backgroundColor: 'lightgray'} : {backgroundColor: 'white'}}>
                                            {pourPrice.pour_size?.size}
                                    </TableCell>
                                    <TableCell 
                                        component="th" scope="row" align="center"
                                        style={(index%2 != 0) ? {backgroundColor: 'lightgray'} : {backgroundColor: 'white'}}>
                                            ${formatter.format(pourPrice.pour_price)}
                                    </TableCell>
                                    {props.allowRemove &&
                                            <IconButton aria-label="delete" className={'delete-button'}>
                                                <Delete fontSize="small" 
                                                onClick={() => {
                                                    if (props.onRemove != undefined) {
                                                        props.onRemove(index);
                                                    }
                                                }}/>
                                            </IconButton>
                                    }
                                </TableRow>
                            )}
                            {props.children}
                        </TableBody>
                    </Table>
                </TableContainer>
            }
        </React.Fragment>
    )
}

export default PourPriceTable;