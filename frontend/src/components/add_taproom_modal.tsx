import React from 'react';
import Grid from  '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { APICall, Endpoint } from '../util/apiCall';

class AddTaproomModal extends React.Component<{show: boolean, successfulCreation: any, unsuccessfulCreation: any}, {name: string}> {

	constructor(props=[] as any) {
		super(props);
		this.state = {
			name: ""
		};
		this.handleSubmit = this.handleSubmit.bind(this);
		this.handleNameChange = this.handleNameChange.bind(this);
	}

	handleSubmit() {
		if(this.state.name === "") {
			this.props.unsuccessfulCreation()
			return;
		}

		//make sure that it isn't empty
		var data = new FormData();
		data.append("name", this.state.name);

		APICall.to(Endpoint.Groupings).post(data)
		.then(() => {
			this.props.successfulCreation()
		}).catch((e) =>{
			this.props.unsuccessfulCreation()
		});
	}

	handleNameChange = (e: React.SyntheticEvent) => {
		const name = (e.target as HTMLInputElement).value;
		this.setState({name: name});
	}


	render() {
		if(!this.props.show){
          return null;
      	}
		return <div>
		<div id="modal">
	        <Grid container direction="row" justify="center" alignItems="center">
	          <h2>Create Taproom:</h2>
	            <TextField value={this.state.name} id="taproom-name" label="Taproom Name" onChange={this.handleNameChange}/>
	        </Grid>
	        <Grid container direction="row" justify="center">
	        	<Button id="save-button" variant="contained" color="primary" onClick={this.handleSubmit}>Save</Button>
	        </Grid>
      	</div>
		</div>
	}
}
export default AddTaproomModal
