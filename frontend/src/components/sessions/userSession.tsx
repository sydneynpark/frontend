export type User = {
    firstName: string
    lastName: string
    email: string
    id: string
}

var UserSession = {
    
    setUser(user: User) {
        sessionStorage.setItem('user', JSON.stringify(user))
        sessionStorage.isAuthenticated = true;
    },

    getUser(){
        let user = sessionStorage.getItem("user")
        if(user){
            return JSON.parse(user);
        }
        return {}
    },

    clearUser(){
        sessionStorage.clear();
    },

    isAuthenticated(){
        return sessionStorage.isAuthenticated || false;
    }
}

export default UserSession