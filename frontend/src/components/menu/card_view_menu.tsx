import React from 'react';
import { Grid, createMuiTheme, ThemeProvider, Typography, } from '@material-ui/core'
import BeverageCard from './beverage_card';
import { STORAGE_NAME_TO_FONT_FAMILY } from '../common/font-selector';
import '../../css/components/menu/menu.css';


type CardViewMenuProps = {
    style: card_style
    beers: beverage[],
    scaling?: number;
}

const CardViewMenu: React.FC<CardViewMenuProps> = (props) => {

    //Creates the beverage cards that go in each row. will create beverage cards equal to the number of colums
    function createColumns(row: number) {

        var columns = [];

        var cardWidth = 12 / props.style.columns;

        for (var j = 0; j < props.style.columns; j++) {

            var beerToDisplay = null;
            if (row * props.style.columns + j < props.beers.length) {
                beerToDisplay = props.beers[(row * props.style.columns) + j]
            }
            columns.push(<Grid container justify="center" key={`row${row}col${j}`} spacing={0} item xs={(cardWidth as any)} >
                <BeverageCard
                    filter={(row % 2 == 1)}
                    beverage={beerToDisplay}
                    color={props.style.beverage_card_color}
                    font={props.style.text_font}
                    titleFont={props.style.title_font}
                    include_description={props.style.include_description}
                    include_pours={props.style.include_pours}
                    include_producer={props.style.include_producers}
                />
            </Grid>)
        }
        return columns;
    };

    //Creates the rows of beverage cards.
    function createRows() {
        var rows = [];
        var rowsTrulyNeeded = props.beers.length >= (props.style.rows * props.style.columns)
            ? props.style.rows
            : Math.ceil(1.0 * props.beers.length / props.style.columns);

        for (var i = 0; i < rowsTrulyNeeded; i++) {
            rows.push(
                <Grid container justify="center" item key={`row${i}`} xs={12} spacing={0} className={'row'} style={{ margin: 0 }}>
                    {createColumns(i)}
                </Grid>);
        }
        return rows;
    };

    return (
        <Grid container spacing={0} className={'beer-container'} alignItems="stretch" justify="center">
            {createRows()}
        </Grid>
    );

}

export default CardViewMenu;