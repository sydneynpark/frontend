import React from 'react';
import { Grid, createMuiTheme, ThemeProvider, Typography, } from '@material-ui/core'
import BeverageCard from './beverage_card';
import {STORAGE_NAME_TO_FONT_FAMILY} from '../common/font-selector';
import '../../css/components/menu/menu.css';
import CardViewMenu from './card_view_menu';
import GridViewMenu from './grid_view_menu';

export type MenuProps = {
    style: menu_style_options,
    beers: beverage[],
    scaling?: number;
};


const Menu: React.FC<MenuProps> = (props) => {

    let scaleFactor = props.scaling || 1;
    const theme = createMuiTheme({
        typography: {
            //This is the menu header
            h1: {
              fontFamily: STORAGE_NAME_TO_FONT_FAMILY.get(props.style.title_font),
              fontSize: `${8*scaleFactor}vh`,
              color: 'white',
            },
            //This is the beverage names
            h2: {
                fontFamily: STORAGE_NAME_TO_FONT_FAMILY.get(props.style.title_font),
                fontSize: `${3*scaleFactor}vh`,
                color: 'white',
            },
            //This is the beverage info
            body1: {
                fontFamily: STORAGE_NAME_TO_FONT_FAMILY.get(props.style.text_font),
                fontSize: `${2.5*scaleFactor}vh`,
                color: 'white',
            },
            //This is the pours info
            body2: {
                fontFamily: STORAGE_NAME_TO_FONT_FAMILY.get(props.style.text_font),
                fontSize: `${2.5*scaleFactor}vh`,
                color: 'grey',
            },
          },
    });

    var backgroundStyling = props.style.use_background_image === true ? {
        'background-image': `url(${props.style.background_image})`
    } : {
        background: props.style.background_color
    }

    props.beers.sort(function(a, b){
        if(a.name < b.name) { return -1; }
        if(a.name > b.name) { return 1; }
        return 0;
    })

    return (<ThemeProvider theme={theme}>

        <div className={'menu-container'} style={backgroundStyling}>
            <div className={'header'}>
                <Typography variant="h1" align="center" style={{height: '100%', padding: '15px'}}>{props.style.title}</Typography>
            </div>
            <div className={'beverages'}>
                {props.style.display_type === "card" && <CardViewMenu {...props}/>}
                {props.style.display_type === "grid" && <GridViewMenu {...props}/>}
            </div>
        </div>
    </ThemeProvider>);

}
export default Menu;