import React from 'react';
import { withStyles, Theme, createStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import '../../css/components/menu/grid_view_menu.css';
import HorizontalPoursList from './horizontal_pours_list';


type GridViewMenuProps = {
    style: grid_style
    beers: beverage[],
    scaling?: number;
}


const GridViewMenu: React.FC<GridViewMenuProps> = (props) => {

    const pourSizes: string[] = [];

    props.beers.forEach(beer => beer.pour_prices.forEach(price => {
       if(price.pour_size?.size != undefined && !pourSizes.includes(price.pour_size.size)){
            pourSizes.push(price.pour_size.size);
       }
    }))

    var formatter = new Intl.NumberFormat('en-US',
    {
        style: 'currency', currency: 'USD',
        minimumFractionDigits: 2
    });


    var getPrice = (beer: beverage, size: string): string => {
        let price = beer.pour_prices.find(price => price.pour_size?.size == size);
        if(price != undefined){
            return formatter.format(price.pour_price);
        }
        return "$----";
    }

    return (
        <TableContainer className={"bev-table-container"}>
            <Table className={"bev-table"} size="small">
                <TableHead>
                    <TableRow style={{backgroundColor: props.style.table_header_color}}>
                        <TableCell ><Typography variant={'h2'}>Name</Typography></TableCell>
                        {props.style.include_description && 
                            <TableCell align="left"><Typography variant={'h2'}>Description</Typography></TableCell>}
                        {props.style.include_producers &&
                            <TableCell align="left"><Typography variant={'h2'}>Producer</Typography></TableCell>}
                        <TableCell className={'super-small-cell'} align="left"><Typography variant={'h2'}>ABV</Typography></TableCell>
                        <TableCell className={'super-small-cell'} align="left"><Typography variant={'h2'}>Type</Typography></TableCell>
                        {props.style.include_pours && pourSizes.map(size => {
                            return <TableCell className={'pour-size-cell'} align="left"><Typography variant={'h2'}>{size}</Typography></TableCell>
                        })}
                    </TableRow>
                </TableHead>
                <TableBody>
                {props.beers.map((beer) => (
                        <TableRow key={beer.id}>
                            <TableCell className={'bev-table-cell'}><Typography variant={"body1"}>{beer.name}</Typography></TableCell>
                            {props.style.include_description && 
                                <TableCell className={'bev-table-cell'} align="left"><Typography variant={"body1"}>{beer.description}</Typography></TableCell>}
                            {props.style.include_producers &&
                                <TableCell className={'bev-table-cell'} align="left"><Typography variant={"body1"}>{beer.producer.name}</Typography></TableCell>}
                            <TableCell className={'super-small-cell bev-table-cell'} align="left"><Typography variant={"body1"}>{beer.abv}</Typography></TableCell>
                            <TableCell className={'super-small-cell bev-table-cell'} align="left"><Typography variant={"body1"}>{beer.beverage_type.type_name}</Typography></TableCell>
                            {props.style.include_pours && pourSizes.map(size => {
                                return <TableCell className={'pour-size-cell bev-table-cell'} align="left" ><Typography>{getPrice(beer, size)}</Typography></TableCell>
                            })}
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );

}

export default GridViewMenu;