import React from 'react'
import { Chip } from '@material-ui/core'
import '../../css/components/menu/horizontal_pours_list.css'

export type HorizontalPoursListProps = {
    pours: pour_price[]
}

var formatter = new Intl.NumberFormat('en-US',
    {
        style: 'currency', currency: 'USD',
        minimumFractionDigits: 2
    });

const HorizontalPoursList: React.FC<HorizontalPoursListProps> = (props: HorizontalPoursListProps) => {
    return (
        <React.Fragment>
            {props.pours.map(pour => (
                <Chip key={pour.id} size={'small'} label={`${(pour.pour_size as pour_size).size} | ${formatter.format(pour.pour_price)}`} className={'pour'}/>
            ))}
        </React.Fragment>
    );
}

export default HorizontalPoursList;