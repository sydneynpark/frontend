import React from 'react'
import '../../css/components/menu/beverage_card.css'
import { Grid, Typography, StylesProvider, ThemeProvider } from '@material-ui/core'
import logo from '../../assets/rnogginLogo.png'
import HorizontalPoursList from './horizontal_pours_list'
import { Style } from '@material-ui/icons'

type BeverageCardProps = {
    filter: boolean,
    beverage: beverage | null,
    color: string,
    titleFont: string,
    font: string,
    include_description: boolean,
    include_pours: boolean,
    include_producer: boolean
}

let BeverageCard: React.FC<BeverageCardProps> = (props: BeverageCardProps) => {

    var cardStyles = {
        background: props.color,
        padding: '0px',
        margin: '0px',
    }

    var textAreaStyles = {
        backdropFilter: `${props.filter ?  'brightness(70%)' : 'brightness(100%)'}`
    }

    return (
        <Grid className={'card'} container spacing={3} item xs={12} style={cardStyles}>
           {props.beverage != null &&

            <React.Fragment>
                <Grid item xs={2} style={{ background: props.beverage.color, height: '100%' }}>
                </Grid>
                <Grid item xs={10} sm container direction="row" spacing={0} style={textAreaStyles}>
                    <Grid item container direction="row" spacing={2} xs={12} justify="flex-start" >
                        <Grid item>
                            <Typography className={'beer-name'} variant="h2">
                                {props.beverage.name}
                            </Typography>
                        </Grid>
                        {props.include_producer &&
                            <Grid item >
                                <Typography variant="body2">
                                    {`by ${props.beverage.producer.name}`}
                                </Typography>
                            </Grid>
                        }
                    </Grid>
                    <Grid item xs={12} className={'beverage-type'}>
                        <Typography variant="body1">
                            {`${props.beverage.beverage_type.type_name}        |        ${props.beverage.abv}%`}
                        </Typography>
                    </Grid>
                    {props.include_description &&
                        <Grid item xs={12} className={'beer-description'} >
                            <Typography variant="body2">
                                {props.beverage.description}
                            </Typography>
                        </Grid>
                    }
                    {props.include_pours &&
                        <Grid item xs={12} sm container direction="row" justify="flex-start" className={'pours'}>
                            <HorizontalPoursList
                                pours={props.beverage.pour_prices}
                            />
                        </Grid>
                    }
                </Grid>
                </React.Fragment>
            }
        </Grid >
    );
};

export default BeverageCard;