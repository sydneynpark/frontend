import React from 'react';
import Grid from  '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import CircularProgress from '@material-ui/core/CircularProgress';

class AddProducerModal extends React.Component<{show: boolean, successfulCreation: any, unsuccessfulCreation: any, cancel: any}, {loading: boolean, name: string, description: string}> {

	constructor(props=[] as any) {
		super(props);
		this.state = {
			name: "",
			description: "",
			loading: false
		};
		this.handleSubmit = this.handleSubmit.bind(this);
		this.handleNameChange = this.handleNameChange.bind(this);
		this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
		this.handleClose = this.handleClose.bind(this);
	}

	handleSubmit() {
		if(this.state.name === "" || this.state.description === "") {
			this.props.unsuccessfulCreation()
			return;
		}

		if (!process.env.REACT_APP_BACKEND_API)
			throw "Missing backend api url in environment variables";

		this.setState({...this.state, loading: true})	
		const BACKEND_URL = process.env.REACT_APP_BACKEND_API;
		//make sure that it isn't empty
		var data = new FormData();
		data.append("name", this.state.name);
		data.append("description", this.state.description);


		fetch(BACKEND_URL + '/producers/', {
			method: 'POST',
			body: data
		})
		.then(async (response) => {
			var body = await response.json();
			this.props.successfulCreation(body)
		}).catch((e) =>{
			this.props.unsuccessfulCreation()
		});
	}

	handleNameChange = (e: React.SyntheticEvent) => {
		const name = (e.target as HTMLInputElement).value;
		this.setState({name: name});
	}

	handleDescriptionChange = (e: React.SyntheticEvent) => {
		const description = (e.target as HTMLInputElement).value;
		this.setState({description: description});
	}

	handleClose = (e: React.SyntheticEvent) => {
		this.props.cancel();
	}

	render() {
		if(!this.props.show){
          return null;
      	}
		return <div>
		<Dialog open={this.props.show} aria-labelledby="form-dialog-title" onClose={this.handleClose}>
			<DialogTitle id="form-dialog-title">Add Producer</DialogTitle>
			<DialogContent>
				<TextField value={this.state.name} id="producer-name" label="Producer Name" onChange={this.handleNameChange} fullWidth/>
				<TextField value={this.state.description} id="producer-description" label="Producer Description" onChange={this.handleDescriptionChange} fullWidth/>
			</DialogContent>
			<DialogActions>
				<Button id="cancel-button" variant="contained" color="secondary" onClick={this.handleClose}>Cancel</Button>
				<Button id="save-button" variant="contained" color="primary" onClick={this.handleSubmit} disabled={this.state.loading}>Save</Button>
				{this.state.loading && <CircularProgress size={24}/>}
			</DialogActions>
		</Dialog>
		</div>
	}
}
export default AddProducerModal
