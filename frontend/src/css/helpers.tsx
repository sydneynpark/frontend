import { isMobile } from 'react-device-detect'

function ChangeIfMobile(styleName: string) {
    return (isMobile ? 'mobile-' +  styleName + ' ': '') + styleName;
}

export default ChangeIfMobile;